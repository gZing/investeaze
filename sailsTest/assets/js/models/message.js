define(["backbone"], function (Backbone){
	
	var Message=Backbone.Model.extend({

	urlRoot: '/message', 
	defaults:{
		to:'',
		from: '',
		content:'',
		receiverRead:0,
		receiverArchieved:0,
		archived:0,
		attachment:''
	}
	})
	return Message;

})