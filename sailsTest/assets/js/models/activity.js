define(["backbone"], function(Backbone) {

    var Activity = Backbone.Model.extend({
        urlRoot: '/activity',
        defaults: {
                    initiator : '',
                    parentId : '',
                    parentCollection : 'Globe',
                    activityType : 'like'
        }
    })

    return Activity;

});