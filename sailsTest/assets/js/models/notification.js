define(["backbone"], function(Backbone){
	var NotificationModel = Backbone.Model.extend({
		urlRoot: '/notification',
		defaults: {
			intiator: '',
			from: '',
			title: '',
			notificationRead: false,
			timestamp: ''
		}
	});
	return NotificationModel;
});