define(["backbone"], function(Backbone) {

  var Fund = Backbone.Model.extend({
    urlRoot: '/fund',
    defaults: {
      fundId : '',
      fundFamily : '',
      fundClass : 'Globe',
      aum : 'Globe',
      currentCrisilRank : 1,
      previousCrisilRank : 1
    }
  })

  return Fund;

});