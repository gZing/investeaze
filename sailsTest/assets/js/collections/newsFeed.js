define(["backbone", "models/post", "models/activity"], function (Backbone, PostModel, ActivityModel) {

  var Feed = Backbone.Collection.extend({

    model: function (attrs, options) {
      if (attrs.postType)
        return new PostModel(attrs, options)
      else if (attrs.activityType)
        return new ActivityModel(attrs, options)
    },

    url: function () {
      return '/feed'
    }
  })

  return Feed;

})