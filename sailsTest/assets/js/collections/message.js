define(["backbone", "models/message"], function(Backbone, MessageModel) {

    var Message = Backbone.Collection.extend({
        model: MessageModel,

        url: function () {
            console.log('/message/' + this.url_append)
            return '/message/' + this.url_append;
          },

        fetchCurrent: function (id, callback) {
            
            this.url_append = 'user/' + String(id);
                 this.fetch({
                success: function(obj) {
                    callback(obj);
                }
            });
            
        }

    })

    return Message;

})