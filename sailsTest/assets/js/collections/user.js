define(["backbone", "models/user"], function(Backbone, UserModel) {

    var User = Backbone.Collection.extend({
        model: UserModel,

        url: function () {
            console.log(this.url_append)
            return this.url_append;
          },

        fetchMessageInteractions: function (id, callback) {
            console.log("id is", id);
            this.url_append = 'message/interactions/' + String(id) + '?page=1';
                 this.fetch({
                success: function(obj) {
                    callback(obj);
                }
            });
            
        }

    })

    return User;

})