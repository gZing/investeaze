
require.config({
	paths: {
		"jquery": 'libs/jquery/dist/jquery.min',
		"underscore": 'libs/underscore/underscore',
		"bootstrap": 'libs/bootstrap/dist/js/bootstrap',
		"backbone": 'libs/backbone/backbone',
    	"JSXTransformer": 'libs/react/JSXTransformer-0.10.0',
		"react": 'libs/react/react-with-addons',
		"moment": 'libs/moment/min/moment.min',
		"text": 'libs/text/text',
    	"jsx": "libs/react/jsx",
    	"slider":"libs/plugins/bootstrap-slider",
    	"starrating":"libs/plugins/starrating",
    	"carousel":"libs/plugins/owl.carousel",
    	"customJ":"libs/plugins/jquery-ui-1.9.2.custom.min",
    	"morris":"libs/plugins/morris",
    	"raphael":"libs/plugins/raphael",
    	"rangeSlider":"libs/ion-range-slider/ion.rangeSlider.min"
	},
	"shim" : {
		"backbone" : {
			"deps" : [
			"jquery",
			"underscore"
			],
			"exports" : "Backbone"
		},
		"jquery" : {
			"exports" : "$"
		},
		"underscore" : {
			"exports" : "_"
		},
		"react" : {
      "deps" : [
      "JSXTransformer"
      ],
			"exports" : "React"
		},
		"morris":{
			"deps":[
			"jquery",
			"raphael"
			],
			"exports":"Morris"
		},
    "rangeSlider": {
      "deps": [
        "jquery"
      ],
      "exports": "Slider"
    }
	}
});

require(["backbone", "react", "jsx!router"], function(Backbone, Router) {
	
});