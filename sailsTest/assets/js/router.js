
define(["backbone", "react", "jsx!components/parent/parent"], function(Backbone, React, Parent) {

  var Router = Backbone.Router.extend({
    routes : {
      "": "index",
      "fund": "fund",
      "login": "login",
      "profile":"profile",
      "profile/:id": "profileId",
      "loginForm": "loginForm",
      "message": "message",
	    "fundhouse":"fundhouse",
      "home": "home"
    },
    index : function() {
      this.current = "";
    },
    fund : function() {
      this.current = "fund";
    },
    login : function() {
      this.current = "login";
    },
    profile : function() {
      this.current = "profile"; 
    },
    profileId : function(id) {
      this.current = "profile"; 
      this.id = id;       
    },
    loginForm: function() {
      this.current = "loginForm";
    },
    message: function() {
      this.current = "message";
    },
    fundhouse: function(){
      this.current="fundhouse";
    },
    home: function(){
      this.current="home";
    }
  });

  var router = new Router();
  React.renderComponent(<Parent router={router} />, document.getElementById('container'));

  Backbone.history.start();
 /* if (!Backbone.history._hasPushState) {
  $('body').delegate('a[href^=#]', 'click', function (e) {
    e.preventDefault();
  });
}
  */
});
