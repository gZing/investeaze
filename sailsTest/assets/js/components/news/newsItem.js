define(["react"], function(React){
  var NewsItem = React.createClass({
    showContent: function(newsId){
      console.log(newsId);
      if($('#news-summary-'+ newsId).is(':hidden')){
        $('#news-content-'+ newsId).slideToggle(function(){
          $('#news-summary-'+ newsId).show();
        });
      } else {
        $('#news-content-'+ newsId).slideToggle(function(){
          $('#news-summary-'+ newsId).hide();
        });
      }
    },
    componentDidMount: function(){
      $('.news-content').hide();
    },
    render: function (){
      return (
        <div>
          <div className='col-md-12' id={'news-summary-'+ this.props.item.newsId} onClick={this.showContent.bind('null', this.props.item.newsId)} style={{'margin-top': '15px', 'border-bottom': '1px solid lightgray'}}>
            <div className='col-md-3'>
              <img src={this.props.item.newsImageURL} height="122"/>
            </div>
            <div className='col-md-9'>
              <div style={{'margin-bottom': '10px'}}><strong>{this.props.item.headline}</strong></div>
              <p style={{'font-size': '12px'}}>{this.props.item.description}</p>
            </div>
          </div>
          <div className='col-md-12 news-content' id={'news-content-'+ this.props.item.newsId} style={{'margin-top': '15px', 'border': '1px solid lightgray'}}>
            <div className='col-md-12'>
              <div className='pull-right' onClick={this.showContent.bind(null, this.props.item.newsId)}> X </div>
              <div style={{'margin-bottom': '10px'}}><strong>{this.props.item.headline}</strong></div>
            </div>
            <div className='col-md-12 col-md-offset-3'>
              <img src={this.props.item.newsImageURL}/>
            </div>
            <div className='col-md-12'>
              <p style={{'font-size': '12px'}} >{this.props.item.content}</p>
            </div>
          </div>
        </div>
        )
    }
  });

  return NewsItem;
});