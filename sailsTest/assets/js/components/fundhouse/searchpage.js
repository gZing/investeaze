define(["react", "jsx!components/fundhouse/sidebar", "jsx!components/fundhouse/searchbox"], function(React, Sidebar, SearchBox) {






var SearchPage=React.createClass({




getInitialState:function(){
   return{
    PortfolioType1:false,
    PortfolioType2:false,
    data:[],
   };
},



componentWillMount: function() {

  var data = [
  {name: "Fund 1", Sharpe: "2:3", risk : "0", type:"1"},
  {name: "Fund 2", Sharpe: "4:5", risk:"1", type:"2"},
  {name: "Fund 3", Sharpe: "5:6", risk:"2", type:"1"},
  {name: "Fund 4", Sharpe: "6:7", risk:"3", type:"2"},
  {name: "Fund 5", Sharpe: "2:5", risk:"4", type:"1"},
  {name: "Fund 6", Sharpe: "4:9", risk:"5", type:"2"},
  {name: "Fund 7", Sharpe: "5:7", risk:"6", type:"2"},
  {name: "Fund 8", Sharpe: "6:13", risk:"7", type:"1"},
  {name: "Fund 9", Sharpe: "2:5", risk: "2", type:"1"},
  {name: "Fund 10", Sharpe:"4:11", risk:"3", type:"2"},
  {name: "Fund 11", Sharpe: "5:6", risk:"4", type:"2"},
  {name: "Fund 12", Sharpe: "6:7", risk:"1", type:"1" },
  {name: "Fund 13", Sharpe: "2:9", risk:"2", type:"1"},
  {name: "Fund 14", Sharpe: "4:7", risk:"1", type:"1"},
  {name: "Fund 15", Sharpe: "2:6", risk:"5", type:"2"},
  {name: "Fund 16", Sharpe: "4:7", risk:"5", type:"2"},
  {name: "Fund 17", Sharpe: "2:3", risk:"2", type:"1"},
  {name: "Fund 18", Sharpe: "3:5", risk:"1", type:"1"},
  {name: "Fund 19", Sharpe: "1:6", risk:"0", type:"2"},
  {name: "Fund 20", Sharpe: "2:7", risk:"0", type:"1"},

];

this.setState({
  data:data
})
      
    },



handleUserInput:function(PortfolioType1, PortfolioType2){
  this.setState({

    PortfolioType1:PortfolioType1,
    PortfolioType2:PortfolioType2
  })
},


  render:function(){

    return(

 
    <div className="container-fluid">
      <div className="col-md-3 ">
        <div className="col-md-12" style={{"margin-top":"70px", "font-family":"Arial"}}>
       <Sidebar 
              PortfolioType1={this.state.PortfolioType1} 
              PortfolioType2={this.state.PortfolioType2}
              onUserInput={this.handleUserInput}
              />
        </div>
      </div> 
      <div style={{ "font-family":"Arial"}}>
        <SearchBox 
          data={this.state.data} 
          PortfolioType1={this.state.PortfolioType1} 
          PortfolioType2={this.state.PortfolioType2}
          />
      </div>
    </div>

        
    );
   }

   }
);


return SearchPage;
})
