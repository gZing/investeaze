define (["react", "jsx!components/fundhouse/searchlist"], function(React, SearchList){

var SearchBox=React.createClass({
  render:function(){
    return(
      <div className="col-md-9" style={{"margin-top":"60px"}}>
        <h4>Search Results</h4>
          
        <SearchList 
          data={this.props.data} 
          PortfolioType1={this.props.PortfolioType1} 
          PortfolioType2={this.props.PortfolioType2}
          />        
      </div>

      )
  }
})

return SearchBox;
})
