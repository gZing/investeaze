
define(["react"], function(React) {

  var PortfolioTable = React.createClass({
    render: function() {
      return (
        <div>
          <h4>Static Table</h4>
          <table className="table table-condensed">
            <thead>
              <th>Name</th>
              <th>Name</th>
            </thead>
            <tbody>
              <tr>
                <td>Text can go here</td>
                <td>Text can go here</td>
              </tr>
              <tr>
                <td>Text can go here</td>
                <td>Text can go here</td>
              </tr>
              <tr>
                <td>Text can go here</td>
                <td>Text can go here</td>
              </tr>
              <tr>
                <td>Text can go here</td>
                <td>Text can go here</td>
              </tr>
              <tr>
                <td>Text can go here</td>
                <td>Text can go here</td>
              </tr>
            </tbody>
          </table>
        </div>
      );
    }
  })

  return PortfolioTable;

})