
define(["react", "jsx!components/fund/commentList"], function(React, CommentList) {

  var Commentary = React.createClass({
    render: function() {
      return (
        <div>
          <h4>Portfolio Commentary</h4>
          <CommentList />
        </div>
      );
    }
  })

  return Commentary;

})