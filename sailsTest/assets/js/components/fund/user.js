
define(["react"], function(React) {

    var User = React.createClass({
      getDefaultProps: function () {
        return {
          url: 'http://localhost:8080/api/user/' + this.props.profile,
          image: 'images/users/' + this.props.profile
        };
      },
      getInitialState: function() {
       return {
        caption: '',
        founded: '',
        title: ''
      }
    },
    componentWillMount: function () {    
      $.get('/auth_user', function (data) {
        this.setState(data);
      }.bind(this));
    },
    render: function() {
      return (
        <div className="row">
          <div className="col-md-4">
            <div className="thumbnail">
              <img src="images/users/aditya.jpg" alt=".." className="profileImage" />
              <div className="caption">
                <h4 className="text-center">{this.state.caption}</h4>
              </div>
            </div>
          </div>
          <div className="col-md-offset-1 col-md-6">
            <h3>{this.state.title}</h3>
            <div className="well well-sm text-center">
              <span className="someClass">Founded: </span>
              <span className="someClass">{this.state.founded}</span>
            </div>
          </div>
        </div>
      );
    }
  });

return User;

})
