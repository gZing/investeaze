
define(["react"], function(React) {

  var Message = React.createClass({
    render: function() {
      return (
        <div>
          <div className="row">
            <div className="col-md-6 text-center">
             <button type="button" className="btn btn-default">Default</button>
             <button type="button" className="btn btn-default btn-info marginl10">Private</button>
            </div>
            <div className="col-md-6 text-center">
              <span className="label label-success">Value</span>
              <span className="label label-success marginl10">Macro</span>
              <span className="label label-success marginl10">Technicals</span>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6 text-center">
              <br/>Some Stuff goes here
            </div>
            <div className="col-md-6 text-center">
              <button type="button" className="btn btn-default btn-lg">Commit</button>
            </div>
          </div>
        </div>
      );
    }
  })

  return Message;

})