
define(["react", "jsx!components/navbar/navbar", "jsx!components/fund/user", "jsx!components/fund/message", "jsx!components/fund/portfolioInfo", "jsx!components/fund/portfolioTable", "jsx!components/fund/metrics", "jsx!components/fund/chart", "jsx!components/fund/holdings", "jsx!components/fund/commentary"], function(React, Navbar, User, Message, PortfolioInfo, PortfolioTable, Metrics, Chart, Holdings, Commentary) {

  var Fund = React.createClass({
    componentWillMount: function() {
     /* $.get( 
       "/auth_user",
        function(data) {
          if(!data.login) {
            window.location = "#/login"
          }
        }
      );*/
    },
    render : function() {
      return (
        <div>
          <Navbar />
          <div className="row-fluid">
            <div className="col-md-offset-1 col-md-10">
              <div className="row">
                <div className="well well-sm col-md-6">
                  <User profile={'535f722304443cf71ab7216c'} />
                </div>
                <div className="well col-md-6">
                  <Message />
                </div>
              </div>
              <div className="row">
                <div className="col-md-9">
                  <PortfolioInfo />
                </div>
                <div className="col-md-3">
                  <PortfolioTable />
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <Metrics />
                </div>
              </div>
              <div className="row">
                <div className="col-md-3">
                  <Chart />
                </div>
                <div className="col-md-3">
                  <Holdings />
                </div>
                <div className="col-md-6">
                  <Commentary />
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
  });

  return Fund;

})