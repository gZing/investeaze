define(["react", "jsx!components/main/main", "jsx!components/index/index", "jsx!components/fund/fund", "jsx!components/login/login", "jsx!components/profile/profile", "jsx!components/misc/firstLoginForm", "jsx!components/message/message","jsx!components/fundhouse/fundhouse"], function(React, MainComponent, Index, Fund, Login, Profile, FirstLoginForm, Message, Fundhouse) {

	var Parent = React.createClass({
		getInitialState: function() {
			return {
				user_id: '',
				visitor_id: '',
				loggedIn: false,
			}
		},
		componentUpdate: function() {
			this.forceUpdate();
		},

		componentWillUpdate: function(){
			var that = this;
			$.get( 
        	"/auth_user",
            function(data) {
              
              if (!data.login) {
            	window.location = "#/login"
          	  }
            }
        	);
		},

		setUserId: function(id) {
			this.setState({user_id: id});
			this.forceUpdate();
		},
		setVisitorId: function(id) {
			this.setState({visitor_id: id});
		},
		componentWillMount : function() {
			var user_id= "";
			var that = this;
			$.get( 
        	"/auth_user"
      ).success(function(data){
          if(data.login) {
            console.log("User Currently Logged In: ", data.userId);
            that.setState({user_id : data.userId })

            //Initialize Pusher once for all, can be accessed in window.pusher
            // and window.channel throughout the application
            console.log("Initialized Pusher from Parent Component - User Id: ", data.userId);
            window.pusher = new Pusher('1d21f8b62586ce0ac86f', { authEndpoint: '/auth/pusher' });
            window.puhserChannel = pusher.subscribe('presence-notificationChannel-' + String(data.userId));
            window.pusher.connection.bind('error', function (err) {
              console.log(err);
            });
          } else {
            window.location = "#/login"
          }
        });

			this.props.router.on("route", this.componentUpdate);
		},
		componentWillUnmount : function() {
			this.props.router.off("route", this.componentUpdate);
		},
		render : function() {
			var userId = this.state.user_id;
			switch(this.props.router.current) {
				case "": return <Index user_id={this.state.user_id} />
				case "fund": return <Fund />
				case "login": return <Login setupUser={this.setUserId}/>
				case "message": return <Message user_id={this.state.user_id} />
				case "profile": return <Profile user_id={userId} param_id ={ this.props.router.id } />
				case "loginForm":	return <FirstLoginForm user_id={this.state.user_id}/>
				case "fundhouse": return <Fundhouse />
				case "home": return <MainComponent user_id={userId} />
				default: return (<div className="text-center"><h3>Invalid Path</h3></div>);
			}
		}
	});

	return Parent;

})
