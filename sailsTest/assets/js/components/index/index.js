
define(["react", "jsx!components/index/slider", "jsx!components/misc/post", "models/user", "jsx!components/index/featuredpost", "jsx!components/index/statusbox", "collections/newsFeed"], function(React, Slider, Post, User, FeaturedPost, StatusBox, NewsFeed) {

  var Index = React.createClass({
    getDefaultProps: function() {
      return {
        activeButton: "btn btn-info",
        inactiveButton: "btn btn-default",
        initiator: this.props.user_id
      }
    },
    getInitialState: function() {
      return {
        sliderItems: [],
        postItems: [],
        headlines: [],
        viewPost: "all",
        user: {}
      }
    },

    fetchFeed: function(){
      var feed = new NewsFeed();
      var that = this;
      feed.fetch({
                success: function(response) {
                  that.setState({ postItems : response}, function(){
                  });
                }
              })
    },

    slideRight: function() {
      var left = $('#main-content > .row-fluid').scrollLeft();
      var toScroll = parseInt($('#main-content > .row-fluid > div').css('width')) + left;
      $('#main-content > .row-fluid').animate({scrollLeft: toScroll }, 800, function() {
        var firstDiv = $('#sliderDiv>div:first');
        firstDiv.parent().append(firstDiv);
        var toScroll = parseInt($('#main-content > .row-fluid > div').css('width'))
        $('#main-content > .row-fluid').scrollLeft(toScroll);
      });
    },
    slideLeft: function() {
      var left = $('#main-content > .row-fluid').scrollLeft();
      var toScroll = parseInt($('#main-content > .row-fluid > div').css('width'));
      $('#main-content > .row-fluid').animate({scrollLeft: left - toScroll}, 800, function() {
        var lastDiv = $('#sliderDiv>div:last');
        lastDiv.parent().prepend(lastDiv);
        $('#main-content > .row-fluid').scrollLeft(toScroll);
      });
    },
    expandStatusBox: function(e) {
      e.target.setAttribute("rows","3");
    },
    filterPost: function(e) {
      this.setState({
        viewPost: e.target.getAttribute('data-filter')
      });
    },
    componentWillMount: function() {
      var that = this;
      console.log("chaching chaching")
      console.log("******************")
      $.get(
      '/auth_user',
      function(data) {
        if(data.login) {
          //user is logged in
          that.fetchFeed();
          var user = new User();
          user.set({id: data.userId});
          user.fetch({
              success: function (model, response, options) {
             
              that.setState({user: response});
              
              }.bind(this) ,

              error: function(model, response, options){
                
              }
          });
        }

        else{
          window.location = "#/login"
        }

      });

      
      /* 
      get slider items from the server
      json format: [{"id":"x", header:"data", rows:[{"id":"n","column1":"Column 1","column2":"Column 2"},{...}]}, {...}]
      */
      var sliderItemsObj = [
        {
          "id":"1",
          "header":"Header1",
          "rows": [
            {"id":"1","column1":"Column 1","column2":"Column 2"},
            {"id":"2","column1":"Column 1","column2":"Column 2"}
          ]
        },
        {
          "id":"2",
          "header":"Header2",
          "rows": [
            {"id":"1","column1":"Column 1","column2":"Column 2"},
            {"id":"2","column1":"Column 1","column2":"Column 2"}
          ]
        },
        {
          "id":"3",
          "header":"Header3",
          "rows": [
            {"id":"1","column1":"Column 1","column2":"Column 2"},
            {"id":"2","column1":"Column 1","column2":"Column 2"}
          ]
        },
        {
          "id":"4",
          "header":"Header4",
          "rows": [
            {"id":"1","column1":"Column 1","column2":"Column 2"},
            {"id":"2","column1":"Column 1","column2":"Column 2"}
          ]
        },
        {
          "id":"5",
          "header":"Header5",
          "rows": [
            {"id":"1","column1":"Column 1","column2":"Column 2"},
            {"id":"2","column1":"Column 1","column2":"Column 2"}
          ]
        },
      ];
      var headlines = [
        {
          "id":"1",
          "name":"Fund VI",
          "invested":"300%",
          "time":"3 days"
        },
        {
          "id":"2",
          "name":"Fund VII",
          "invested":"200%",
          "time":"1 days"
        },
        {
          "id":"3",
          "name":"Fund VIII",
          "invested":"333%",
          "time":"2 days"
        }
      ]
      this.setState({
        sliderItems: sliderItemsObj,
        headlines: headlines
      });
    },
    componentDidMount: function() {
      var toScroll = parseInt($('#main-content > .row-fluid > div').css('width'));
      $('#main-content > .row-fluid').scrollLeft(toScroll);
    },
    render: function() {

      var sliderItems = this.state.sliderItems.map(function (item) {
        return <Slider header={item.header} rows={item.rows} key={item.id} />;
      });
      var postItems = this.state.postItems.map(function (post) {
        if(this.state.viewPost == "all")
          return post.featured ? <FeaturedPost data={post} key={post.id} /> : <Post data={post} key={post.id} />;
        else if(this.state.viewPost == post.type)
          return <Post data={post} key={post.id} />;
      }.bind(this));
      var headlines = this.state.headlines.map(function(news) {
        return (
          <div className="media" key={news.id}>
            <a className="pull-left text-center" href="#">
              <h4>{news.name}</h4>
            </a>
            <div className="media-body">                  
              {news.invested} invested.<br />{news.time} left.
            </div>
            <hr />
          </div>
        );
      });
      return (
        <div>
          <div className="row-fluid">
            <div className="jumbotron text-center">
              <h1>Index Page</h1>
              <a href="#/fund" className="btn btn-primary">Fund Page</a>
              <a href="#/login" className="btn btn-warning">Login Page</a>
              <a href="#/profile" className="btn btn-success">Profile Page</a>
            </div>
          </div>

          <div className="row-fluid">
            <div className="col-md-1 text-right">
              <span className="fa fa-chevron-circle-left" onClick={this.slideLeft}></span>
            </div>
            <div className="col-md-10">
              <div id="main-content">
                <div className="row-fluid" id="sliderDiv">
                  {sliderItems}
                </div>            
              </div>          
            </div>
            <div className="col-md-1 text-left">
              <span className="fa fa-chevron-circle-right" onClick={this.slideRight}></span>
            </div>
          </div>

          <div className="row-fluid">
            <div className="col-md-12"><hr /></div>
          </div>

          <div className="row-fluid">
            <div className="col-md-offset-1 col-md-8">
              <div className="row-fluid">
                <div className="col-md-12">
                  <button type="button" className={this.state.viewPost == "all" ? this.props.activeButton : this.props.inactiveButton} onClick={this.filterPost} data-filter="all">All</button>
                  <button type="button" className={this.state.viewPost == "fund" ? this.props.activeButton : this.props.inactiveButton} onClick={this.filterPost} data-filter="fund">Funds</button>
                  <button type="button" className={this.state.viewPost == "following" ? this.props.activeButton : this.props.inactiveButton} onClick={this.filterPost} data-filter="following">Following</button>
                  <button type="button" className={this.state.viewPost == "idea" ? this.props.activeButton : this.props.inactiveButton} onClick={this.filterPost} data-filter="idea">Ideas</button>
                </div>
              </div>
              <div className="row-fluid">
                <div className="col-md-10 margint10 marginb20">
                  <StatusBox user_id = { this.props.user_id} />
                </div>
                <div className="col-md-2 margint10 marginb20">
                  <button className="btn btn-primary">Post Update</button>
                </div>
              </div>
              {postItems}
            </div>
            <div className="col-md-2 well well-sm">
              <strong>Upcoming headlines</strong>
              {headlines}
            </div>
          </div>
        </div>
      );
    }
  });

  return Index;

});