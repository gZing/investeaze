define (["react", "models/post"], function(React, PostModel){
	
	var StatusBox=React.createClass({
	   setActive: function(e) {
      this.setState({activeTab: e.target.innerHTML});
    },
    getInitialState: function() {
      return {
        postItems: [],
        activeTab: '',
        status:'',
        ticker:'',
        advice:'',
        trend:'',
        amount:'',
        deadline:'',
        maxPeople:''

      };
    },


    getDefaultProps: function() {
      return {
        activeTabPane: "tab-pane active"
      }
    },

    

    handlePostChange: function(field, e) {
      var nextState = {};
      nextState[field] = e.target.value;
      this.setState(nextState);
    },

    sendPost: function() {
      var postModel = new PostModel();
      if (this.state.activeTab == "Status"){
        postModel.save(
        {
          initiator: this.props.user_id,
          postType: this.state.activeTab,
          content: this.state.status,

        },
        {
          url: '/post',
          success: function(model, response, options) {
            this.state.postItems.unshift(model);
            this.setState({postItmes: this.state.postItems, postText: ''});
          }.bind(this),
          error: function() {
            /* POST COULD NOT BE UPDATED */
          }
        }
      )
      }

      if(this.state.activeTab == "Pool"){
        postModel.save(
        {
          initiator: this.props.user_id,
          postType: this.state.activeTab,
          content: this.state.activeTab,
          ticker: this.state.ticker,
          amount: this.state.amount,
          maxPeople: this.state.maxPeople

        },
        {
          url: '/post',
          success: function(model, response, options) {
            this.state.postItems.unshift(model);
            this.setState({postItmes: this.state.postItems, postText: ''});
          }.bind(this),
          error: function() {
            /* POST COULD NOT BE UPDATED */
          }
        }
      )

      }

      if(this.state.activeTab == "Idea"){
        postModel.save(
        {
          initiator: this.props.user_id,
          postType: this.state.activeTab,
          content: this.state.status,
          ticker: this.state.ticker,
          advice: this.state.advice,
          trend: this.state.trend


        },
        {
          url: '/post',
          success: function(model, response, options) {
            this.state.postItems.unshift(model);
            this.setState({postItmes: this.state.postItems, postText: ''});
          }.bind(this),
          error: function() {
            /* POST COULD NOT BE UPDATED */
          }
        }
      )

      }
      
    },

	render: function(){
    console.log(this.props.user_id)
	return(

    <div style={{"border":"1px solid silver"}} >
    <ul className="nav nav-tabs" >


        <li className={ this.state.activeTab == 'Status' ? "active" : "" }><a className="pointer" onClick={this.setActive} >Status</a></li>
            
            <li className={ this.state.activeTab == 'Idea' ? "active" : "" }><a className="pointer" onClick={this.setActive} >Idea</a></li>
            
            <li className={ this.state.activeTab == 'Pool' ? "active" : "" }><a className="pointer" onClick={this.setActive}>Pool</a></li>
                  
        </ul>

        <div className="tab-content">
        <div className={ this.state.activeTab == 'Status' ? this.props.activeTabPane : "tab-pane" } >
          <div className="col-md-11 input-group" style={{"margin":"5px"}}>
            <textarea name="status" className="form-control" rows="2" placeholder="Write something..." onChange={ this.handlePostChange.bind(this, 'status') } ></textarea>
            <button type="button" className="pull-right" onClick={this.sendPost} value="Add" style={{"margin-top":"8px"}}>Post</button> 
          </div>
        </div>
        <div className={ this.state.activeTab == 'Idea' ? this.props.activeTabPane : "tab-pane" } >
          <div className="col-md-11 input-group" style={{"margin":"5px"}}>
            <div>
              <div className="col-md-3" > Ticker:</div>
              <div className="input-group input-group-sm col-md-4" style={{"margin":"5px"}}>
                <input name="ticker" type="text" className="form-control" onChange={this.handlePostChange.bind(this, 'ticker')} />
              </div>
            </div>
            <div>
              <div className="col-md-3" > Buy/Long/Sell:</div>
              <div className="input-group input-group-sm col-md-4" style={{"margin":"5px"}}>
                <input name="advice" type="text" className="form-control" onChange={this.handlePostChange.bind(this, 'advice')} />
              </div>
            </div>
            <div>
              <div className="col-md-3" > Bullish/Bearish:</div>
              <div className="input-group input-group-sm col-md-4" style={{"margin":"5px"}}>
                <input name="trend" type="text" className="form-control" onChange={this.handlePostChange.bind(this, 'trend')}/>
              </div>
            </div>
                <textarea name="status" className="form-control" rows="2" placeholder="Write something..." onChange={this.handlePostChange.bind(this, 'status') }></textarea>
              <button type="button" className="pull-right" onClick={this.sendPost} value="Add" style={{"margin-top":"8px"}}>Post</button> 
          </div>
        </div>
        <div className={ this.state.activeTab == 'Pool' ? this.props.activeTabPane : "tab-pane" } >
          <div className="col-md-11 input-group" style={{"margin":"5px"}}>
            <div>
              <div className="col-md-3" > Ticker:</div>
              <div className="input-group input-group-sm col-md-4" style={{"margin":"5px"}}>
                <input name="ticker" type="text" className="form-control" onChange={this.handlePostChange.bind(this, 'ticker')}/>
              </div>
            </div>
            <div>
              <div className="col-md-3" > Amount:</div>
              <div className="input-group input-group-sm col-md-4" style={{"margin":"5px"}}>
                <input name="amount" type="text" className="form-control" onChange={this.handlePostChange.bind(this, 'amount')}/>
              </div>
            </div>
            <div>
              <div className="col-md-3" > Deadline:</div>
              <div className="input-group input-group-sm col-md-4" style={{"margin":"5px"}}>
                <input name="deadline" type="text" className="form-control" onChange={this.handlePostChange.bind(this, 'deadline')}/>
              </div>
            </div>
            <div>
              <div className="col-md-3" > Max people:</div>
              <div className="input-group input-group-sm col-md-4" style={{"margin":"5px"}}>
                <input name="maxPeople" type="text" className="form-control" onChange={this.handlePostChange.bind(this, 'ticker')}/>
              </div>
            </div>
            
            <textarea name="status" id="in" className="form-control" rows="2" placeholder="Write something..." onChange={this.handlePostChange.bind(this, 'status')} ></textarea>
            <button type="button" className="pull-right" onClick={this.sendPost} value="Add" style={{"margin-top":"8px"}}>Post</button> 
          </div>
        </div>
        </div>
        </div>



    

    )
	}

	})


	return StatusBox;

})