define(["react", "collections/notification", 'moment'], function (React, NotificationCollection, moment) {

  var NotificationRow = React.createClass({
    handleClick: function(notificationId){
      console.log(notificationId);


    },
    render: function () {
      var row;
      switch (this.props.item.activityType) {
        case 'Like':
          row = <span className="name">{this.props.item.E1} liked {this.props.item.parentCollection} by {this.props.item.E2} </span>;
          break;
        case 'Share':
          row = <span className="name"> {this.props.item.E1} shared this {this.props.item.parentCollection} </span>;
          break;
        case 'Refer':
          row = <span className="name">{this.props.item.E1} posted this {this.props.item.parentCollection} </span>;
          break;
        case 'Invest':
          row = <span className="name"> {this.props.item.E1} invested in {this.props.item.E2} </span>;
          break;
        case 'Follow':
          row = <span className="name"> {this.props.item.E1} followed  {this.props.item.E2} </span>;
          break;
        case 'Comment':
          row = <span className="name"> {this.props.item.E1} commented on {this.props.item.parentCollection} by {this.props.item.E2} </span>;
          break;
        case 'Post':
          row = <span className="name"> {this.props.item.E1} posted on of {this.props.item.E2} </span>;
          break;
        default:
          break;
      }

      return (
        <div className="panel" style={{"margin-bottom": "5px"}} >
          <div className="panel-body">
            <div className="dir-info">
              <div className="row">
                <div className="col-xs-2">
                  <div className="avatar">
                    <img src="/images/postImg2.jpg" alt="" onClick={this.props.showUserProfile.bind(null, this.props.item.initiator)}/>
                  </div>
                </div>
                <div className="col-xs-7">
                  <h5>{row}</h5>
                </div>
                <div className="col-xs-3">
                  <a className="dir-like" href="javascript:void(0);">
                    <span className="small">{moment(this.props.item.createdAt).fromNow()}</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        );
    }
  });


  var NotificationPanel = React.createClass({
    getInitialState: function () {
      return {
        notifications: [],
        notificationInitialized: false,
        userId: this.props.userId
      }
    },
    getNotifications: function (id) {
      var that = this;
      console.log("Get Notifications for User: ", id);
      var notification = new NotificationCollection();
      notification.fetchList(id, function (data) {
        that.setState({notifications: data});
        $('.notification-count').trigger('notificationCountUpdate', {notification: data});
      });
    },
    addNotificationToPanel: function (data) {
      this.state.notifications.push(data);
      $('.notification-count').trigger('notificationCountUpdate', {notification: data});
      this.setState({ notifications: this.state.notifications});
    },
    bindPusherNotificationChannel: function () {
      window.puhserChannel.unbind('newNotification', this.addNotificationToPanel);
      window.puhserChannel.bind('newNotification', this.addNotificationToPanel);
    },
    initPushNotifications: function (id) {
      var that = this;
      if (!this.state.notificationInitialized) {
        console.log('Pusher Notification Initialized... for user :', id);
        if (window.pusher && window.pusherChannel) {
          this.bindPusherNotificationChannel(id);
        } else {
          window.pusher = new Pusher('1d21f8b62586ce0ac86f', { authEndpoint: '/auth/pusher' });
          window.puhserChannel = pusher.subscribe('presence-notificationChannel-' + String(id));
          window.pusher.connection.bind('error', function (err) {
            console.log(err);
          });
          this.bindPusherNotificationChannel(id);
        }
        this.state.notificationInitialized = true;
      }
    },
    componentWillMount: function () {
      /*Initial component bindings*/
    },
    componentDidMount: function(){
      this.getNotifications(this.props.userId);
    },
    componentWillReceiveProps: function (nextProps) {
      if (nextProps.userId) {
        this.initPushNotifications(nextProps.userId);
        this.getNotifications(nextProps.userId);
      }
    },
    render: function () {
      var notifications = [];
      var that = this;

      if (this.state.notifications.length) {
        this.state.notifications.sort(function(a, b){
          if(+new Date(a.createdAt) > +new Date(b.createdAt)){
            return 1;
          } else {
            return -1;
          }
        });

        notifications = this.state.notifications.map(function (notification, idx) {
          return <NotificationRow item={notification} key={idx} showUserProfile={that.props.showUserProfile}/>
        });
      }
      notifications.sort(function(a, b){
        if(+new Date(a.createdAt) > +new Date(b.createdAt)){
          return 1;
        } else {
          return -1;
        }
      });
      return (
        <div className="social">
          <div style={{"font-weight": 'bold'}}> Notifications </div>
          {notifications}
        </div>
        );
    }
  });

  return NotificationPanel;
});