define(["react", "jsx!components/message/chatlist", "jsx!components/message/chatbox", "collections/message", "collections/user"], function(React, ChatList, ChatBox, MessageCollection, UserCollection) {

  var Message = React.createClass({

    getInitialState : function(){
      console.log("error reporting 1")
      return { 
        data : [],
        chatBox : false
      }     
      
    },

    fetchList: function(id) {

      var userCollection = new UserCollection();

            var user = userCollection.fetchMessageInteractions(id, function(interactions){
                    var activeUser = interactions.models[0] ? interactions.models[0].get('with')
                                                            : null
                    console.log(interactions)
                    this.setState({
                      data: interactions,
                      activeUser: activeUser
                    });
            }.bind(this)
            );
    },

    onChatNameClick: function(id){
      this.setState({activeUser: id, chatBox : true})
    },

    setListFlag: function(){
      this.setState({ chatBox : false })
    },

    componentWillMount: function() {

      this.fetchList(this.props.userId);
      
    },

    componentWillReceiveProps: function(newProps, newState) {
      this.fetchList(newProps.userId);
    },

    render: function() {
      console.log(this.state.chatBox);
      console.log("((((((((()))))))))");
      return(

        <div className="panel">
             { this.state.chatBox ? <ChatBox userId ={this.props.userId} userObj={this.props.userObj} activeUser={this.state.activeUser} setListFlag={this.setListFlag} />
                                  : <ChatList userId ={this.props.userId} activeUser={this.props.activeUser} onUserNameClick={this.onChatNameClick} data={this.state.data} /> }
        </div>
      )
    }
  })

return Message;

})

    