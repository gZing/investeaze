define(["react", "morris"], function(React, Morris){
  var FundDetails = React.createClass({
    getInitialState: function(){
      return {
        newsItems: []
      }
    },

    clicked: function(){
      console.log("clicked")

    },

    componentDidMount:function(){




      $( "ul#item-2" ).toggle( "blind");
      $( "ul#item-3" ).toggle( "blind");
      $( "ul#item-4" ).toggle( "blind");
      $( "ul#item-5" ).toggle( "blind");

      $( "#item1" ).click(function() {
        $( "ul#item-1" ).toggle( "blind");
        $( "ul#item-2" ).hide( "blind");
        $( "ul#item-3" ).hide( "blind" );
        $( "ul#item-4" ).hide( "blind" );
        $( "ul#item-5" ).hide( "blind" );
      });

      $( "#item2" ).click(function() {
        $( "ul#item-2" ).toggle( "blind");
        $( "ul#item-1" ).hide( "blind");
        $( "ul#item-3" ).hide( "blind" );
        $( "ul#item-4" ).hide( "blind" );
        $( "ul#item-5" ).hide( "blind" );
      });

      $( "#item3" ).click(function() {
        $( "ul#item-3" ).toggle( "blind" );
        $( "ul#item-2" ).hide( "blind");
        $( "ul#item-1" ).hide( "blind" );
        $( "ul#item-4" ).hide( "blind" );
        $( "ul#item-5" ).hide( "blind" );
      });

      $( "#item4" ).click(function() {
        $( "ul#item-4" ).toggle( "blind" );
        $( "ul#item-2" ).hide( "blind");
        $( "ul#item-3" ).hide( "blind" );
        $( "ul#item-1" ).hide( "blind" );
        $( "ul#item-5" ).hide( "blind" );
      });

      $( "#item5" ).click(function() {
        $( "ul#item-5" ).toggle( "blind" );

        $( "ul#item-2" ).hide( "blind");
        $( "ul#item-3" ).hide( "blind" );
        $( "ul#item-4" ).hide( "blind" );
        $( "ul#item-1" ).hide( "blind" );
      });

      /*
       $("h4").click(function(){
       console.log("clicked")
       $("ul").
       $ul.next().hide("blind")
       $ul.toggle("blind");
       })*/


      Morris.Bar({
        element: 'bar-example',
        data: [
          { y: '2006', a: 100, b: 90 },
          { y: '2007', a: 75,  b: 65 },
          { y: '2008', a: 50,  b: 40 },
          { y: '2009', a: 75,  b: 65 },
          { y: '2010', a: 50,  b: 40 },
          { y: '2011', a: 75,  b: 65 },
          { y: '2012', a: 100, b: 90 }
        ],
        xkey: 'y',
        ykeys: ['a', 'b'],
        labels: ['Series A', 'Series B']
      });


    },


    render: function (){

      return (
        <div>
          <div className="panel-group " id="accordion">
            <div className="panel">
              <div className="panel-heading">
                <h4 className="panel-title" id="item1">Performance</h4>
              </div>
              <ul id="item-1" >
                <div id="collapseOne" className="panel-collapse collapse in" >
                  <div className="panel-body">

                    <div className="col-sm-12">
                      <section className="panel">

                        <div className="panel-body">

                          <div id="bar-example"></div>
                          <table className="table">
                            <thead>
                              <tr>

                                <th>Crisil Rank</th>
                                <th>Assets (Rs. cr)</th>
                                <th>3mth(%)</th>
                                <th>6mth(%)</th>
                                <th>1yr(%)</th>
                                <th>3yr(%)</th>
                                <th>5yr(%)</th>


                              </tr>
                            </thead>
                            <tbody>
                              <tr>

                                <th>Crisil Rank</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>


                              </tr>
                              <tr>


                                <th>Crisil Rank</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>

                              </tr>
                              <tr>


                                <th>Crisil Rank</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                              </tr>
                              <tr>


                                <th>Crisil Rank</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                              </tr>
                              <tr>


                                <th>Crisil Rank</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                              </tr>
                            </tbody>
                          </table>
                        </div>

                      </section>
                    </div>


                  </div>
                </div>
              </ul>
            </div>

            <div className="panel">
              <div className="panel-heading">
                <h4 className="panel-title" id="item2">Investment Info</h4>
              </div>

              <ul id="item-2">
                <div id="collapseOne" className="panel-collapse collapse in">
                  <div className="panel-body">

                    <p>Type of scheme upto October 10, 2011: A five year close ended diverified equity scheme with an automatic conversion into an
                    open ended scheme upon maturity.Type of scheme on aor afer investemnet the scheme to rovide long term captial by investing
                    predominantly in a diversified portfolio
                    </p>

                    <div className="col-sm-6">
                      <section className="panel">

                        <div className="panel-body">
                          <table className="table">
                            <thead>
                              <tr>

                                <th>Scheme Detail</th>
                                <th></th>

                              </tr>
                            </thead>
                            <tbody>
                              <tr>

                                <td>Investment Plan</td>
                                <td></td>

                              </tr>
                              <tr>

                                <td>Launch Date</td>
                                <td></td>

                              </tr>
                              <tr>

                                <td>Benchmark</td>
                                <td></td>

                              </tr>
                              <tr>

                                <td>Asset Size(Rs cr)</td>
                                <td></td>

                              </tr>
                              <tr>

                                <td>Minimum Investment</td>
                                <td></td>

                              </tr>
                              <tr>

                                <td>Last Dividend</td>
                                <td></td>

                              </tr>
                              <tr>

                                <td>Bonus</td>
                                <td></td>

                              </tr>
                              <tr>

                                <td>Fund Manager</td>
                                <td></td>

                              </tr>
                              <tr>

                                <td>Notes</td>
                                <td>Thornton</td>

                              </tr>
                            </tbody>
                          </table>
                        </div>

                      </section>
                    </div>
                    <div className="col-sm-6">
                      <section className="panel">

                        <div className="panel-body">
                          <table className="table">
                            <thead>
                              <tr>

                                <th>Load Details</th>
                                <th></th>

                              </tr>
                            </thead>
                            <tbody>
                              <tr>

                                <td>Mark</td>
                                <td>Otto</td>

                              </tr>
                              <tr>

                                <td>Jacob</td>
                                <td>Thornton</td>

                              </tr>
                              <tr>

                                <td>Larry</td>
                                <td>the Bird</td>

                              </tr>
                            </tbody>
                          </table>
                        </div>

                      </section>
                    </div>
                    <div className="col-sm-6">
                      <section className="panel">

                        <div className="panel-body">
                          <table className="table">
                            <thead>
                              <tr>

                                <th>Contact Details</th>
                                <th></th>

                              </tr>
                            </thead>
                            <tbody>
                              <tr>

                                <td>Mark</td>
                                <td>Otto</td>

                              </tr>
                              <tr>

                                <td>Jacob</td>
                                <td>Thornton</td>

                              </tr>
                              <tr>

                                <td>Larry</td>
                                <td>the Bird</td>

                              </tr>
                            </tbody>
                          </table>
                        </div>

                      </section>
                    </div>

                  </div>
                </div>

              </ul>
            </div>


            <div className="panel">
              <div className="panel-heading">
                <h4 className="panel-title" id="item3">Peer Comparasion</h4>
              </div>

              <ul id="item-3">
                <div id="collapseOne" className="panel-collapse collapse in">
                  <div className="panel-body">

                    <div className="col-sm-12">
                      <section className="panel">

                        <div className="panel-body">
                          <table className="table">
                            <thead>
                              <tr>

                                <th>Crisil Rank</th>
                                <th>Assets (Rs. cr)</th>
                                <th>3mth(%)</th>
                                <th>6mth(%)</th>
                                <th>1yr(%)</th>
                                <th>3yr(%)</th>
                                <th>5yr(%)</th>


                              </tr>
                            </thead>
                            <tbody>
                              <tr>

                                <th>Crisil Rank</th>
                                <th>Assets (Rs. cr)</th>
                                <th>3mth(%)</th>
                                <th>6mth(%)</th>
                                <th>1yr(%)</th>
                                <th>3yr(%)</th>
                                <th>5yr(%)</th>


                              </tr>
                              <tr>


                                <th>Crisil Rank</th>
                                <th>Assets (Rs. cr)</th>
                                <th>3mth(%)</th>
                                <th>6mth(%)</th>
                                <th>1yr(%)</th>
                                <th>3yr(%)</th>
                                <th>5yr(%)</th>

                              </tr>
                              <tr>


                                <th>Crisil Rank</th>
                                <th>Assets (Rs. cr)</th>
                                <th>3mth(%)</th>
                                <th>6mth(%)</th>
                                <th>1yr(%)</th>
                                <th>3yr(%)</th>
                                <th>5yr(%)</th>
                              </tr>
                              <tr>


                                <th>Crisil Rank</th>
                                <th>Assets (Rs. cr)</th>
                                <th>3mth(%)</th>
                                <th>6mth(%)</th>
                                <th>1yr(%)</th>
                                <th>3yr(%)</th>
                                <th>5yr(%)</th>
                              </tr>
                              <tr>


                                <th>Crisil Rank</th>
                                <th>Assets (Rs. cr)</th>
                                <th>3mth(%)</th>
                                <th>6mth(%)</th>
                                <th>1yr(%)</th>
                                <th>3yr(%)</th>
                                <th>5yr(%)</th>
                              </tr>
                            </tbody>
                          </table>
                        </div>

                      </section>
                    </div>



                  </div>
                </div>

              </ul>
            </div>

            <div className="panel">
              <div className="panel-heading">
                <h4 className="panel-title" id="item4">Portfolio</h4>
              </div>
              <ul id="item-4">
                <div id="collapseOne" className="panel-collapse collapse in">
                  <div className="panel-body">

                    <div className="col-md-12">
                      <div className="col-sm-6">
                        <section className="panel">
                          <h3>Top Holdings</h3>
                          <div className="panel-body">
                            <table className="table">
                              <thead>
                                <tr>

                                  <th>Equity</th>
                                  <th>Sector</th>
                                  <th>Value (Rs cr)</th>
                                  <th>Asset %</th>

                                </tr>
                              </thead>
                              <tbody>
                                <tr>

                                  <td>Investment Plan</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>

                                </tr>
                                <tr>


                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>

                                </tr>
                                <tr>


                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>


                                </tr>
                                <tr>


                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>


                                </tr>
                                <tr>


                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>


                                </tr>
                                <tr>

                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>

                                </tr>
                                <tr>


                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>

                                </tr>
                                <tr>


                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>

                                </tr>
                                <tr>


                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>


                                </tr>
                              </tbody>
                            </table>
                          </div>

                        </section>
                      </div>
                      <div className="col-sm-6">
                        <section className="panel">

                          <div className="panel-body">
                            <table className="table">
                              <thead>
                                <tr>

                                  <th>Scheme Detail</th>
                                  <th></th>

                                </tr>
                              </thead>
                              <tbody>
                                <tr>


                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>


                                </tr>
                                <tr>


                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>


                                </tr>
                                <tr>


                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>


                                </tr>
                                <tr>


                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>

                                </tr>
                                <tr>


                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>


                                </tr>
                                <tr>


                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>

                                </tr>

                              </tbody>
                            </table>
                          </div>

                        </section>
                      </div>
                    </div>
                    <div className="col-md-12">
                      <div className="col-sm-6">
                        <section className="panel">
                          <h3>Asset Allocation</h3>
                          <div className="panel-body">
                            <table className="table">
                              <thead>
                                <tr>

                                  <th>Scheme Detail</th>
                                  <th></th>

                                </tr>
                              </thead>
                              <tbody>
                                <tr>

                                  <td>Investment Plan</td>
                                  <td></td>

                                </tr>
                                <tr>

                                  <td>Launch Date</td>
                                  <td></td>

                                </tr>
                                <tr>

                                  <td>Benchmark</td>
                                  <td></td>

                                </tr>
                                <tr>

                                  <td>Asset Size(Rs cr)</td>
                                  <td></td>

                                </tr>
                                <tr>

                                  <td>Minimum Investment</td>
                                  <td></td>

                                </tr>

                              </tbody>
                            </table>
                          </div>

                        </section>
                      </div>
                      <div className="col-sm-6">
                        <section className="panel">
                          <h3>Concentration</h3>
                          <div className="panel-body">
                            <table className="table">
                              <thead>
                                <tr>

                                  <th>Scheme Detail</th>
                                  <th></th>

                                </tr>
                              </thead>
                              <tbody>
                                <tr>

                                  <td>Investment Plan</td>
                                  <td></td>

                                </tr>
                                <tr>

                                  <td>Launch Date</td>
                                  <td></td>

                                </tr>
                                <tr>

                                  <td>Benchmark</td>
                                  <td></td>

                                </tr>
                                <tr>

                                  <td>Asset Size(Rs cr)</td>
                                  <td></td>

                                </tr>

                              </tbody>
                            </table>
                          </div>

                        </section>
                      </div>
                    </div>




                  </div>
                </div>

              </ul>
            </div>

            <div className="panel">
              <div className="panel-heading">
                <h4 className="panel-title" id="item5">News and Videos</h4>
              </div>
              <ul id="item-5">
                <div id="collapseOne" className="panel-collapse collapse in">
                  <div className="panel-body">

                  News


                  </div>
                </div>
              </ul>
            </div>

          </div>

        </div>
        )
    }
  });

  return FundDetails;
});

