define(["react", 'rangeSlider', 'starrating', 'jsx!components/explore/filterItem'], function (React, Slider, StarRating, FilterItem) {
  var FundFilters = React.createClass({
    getInitialState: function () {
      return {
        filterItems: [],
        currentFilter: {}
      }
    },
    addToFilterCriteria: function (key, value) {
      this.filter = this.filter || {};
      this.filter[key] = value;
      console.log("Criteria Added ->", this.filter);
      this.setState({currentFilter: this.filter});
    },
    removeFromFilterCriteria: function(key){
      if(this.filter && this.filter[key]){
        delete this.filter[key];
        console.log("Criteria Removed ->",this.filter);
        this.setState({currentFilter: this.filter});
      }
    },
    componentWillMount: function () {
      this.setState({filterItems: [
        {
          'fundName': 'SBI Mutual Fund',
          'fundImageURI': '/images/logo-fund.png',
          'fundRating': '10',
          'fundRank': 2,
          'aum': '1244',
          'nav': '1950',
          'performance': {
            'quarter': '5.3',
            'yearly': '13.6',
            'since': '29 May, 2013'
          },
          'followers': [],
          fundId: '2231232'
        },
        {
          'fundName': 'HDFC Mutual Fund',
          'fundImageURI': '/images/logo-fund.png',
          'fundRating': '15',
          'fundRank': 4,
          'aum': '2300',
          'nav': '1989',
          'performance': {
            'quarter': '5.0',
            'yearly': '93.6',
            'since': '29 May, 2013'
          },
          'followers': [],
          fundId: '22312978979'
        }
      ]})
    },
    componentDidMount: function () {
      var that = this;
      $("#performance").ionRangeSlider({
        min: 0,
        max: 40,
        type: 'single',
        from: 10,
        step: 1,
        postfix: "",
        prettify: false,
        hasGrid: true,
        onChange: function(obj){
          that.addToFilterCriteria('performance', obj.fromNumber);
        }
      });
      $("#aum").ionRangeSlider({
        min: 100,
        max: 1500,
        type: 'single',
        step: 500,
        postfix: "",
        prettify: false,
        hasGrid: true,
        onChange: function(obj){
          that.addToFilterCriteria('aum', obj.fromNumber);
        }
      });
      $("#fundage").ionRangeSlider({
        min: 1,
        max: 5,
        type: 'single',
        step: 1,
        postfix: "",
        prettify: false,
        hasGrid: true,
        onChange: function(obj){
          that.addToFilterCriteria('fundage', obj.fromNumber);
        }
      });
      $('.child').hide();
      $('.parent').click(function () {
          $(this).siblings('.parent').find('ul').slideUp();
          $(this).find('ul').show();
      });

      $('input[type=checkbox]').on('change', function(){
        if($(this).data().filter && $(this).is(':checked')){
          that.addToFilterCriteria($(this).data().filter, $(this).val());
        } else {
          that.removeFromFilterCriteria($(this).data().filter);
        }
      });
    },
    render: function () {
      var that = this;
      var filteredItemList = this.state.filterItems.map(function (item, index) {
        return <FilterItem item={item} key={index} userId={that.props.userId} showFundDetails={that.props.showFundDetails}/>
      });

      return (
        <div>
          <div className='col-md-3 filterPanel'>
            <div className='col-md-12' style={{'border-bottom': '1px solid lightgray'}}>
              <ul className='filterList'>
                <li style={{'margin-bottom': '7px'}}>
                  <a href='javascript:void(0);'> Top Performing</a>
                </li>
                <li style={{'margin-bottom': '7px'}}>
                  <a href='javascript:void(0);'> Top Ranked </a>
                </li>
                <li style={{'margin-bottom': '7px'}}>
                  <a href='javascript:void(0);'> Recent Dividends </a>
                </li>
                <li style={{'margin-bottom': '7px'}}>
                  <a href='javascript:void(0);'> NFO </a>
                </li>
                <li style={{'margin-bottom': '7px'}}>
                  <a href='javascript:void(0);'> Trending </a>
                </li>
                <li style={{'margin-bottom': '7px'}}>
                  <a href='javascript:void(0);'>Most Achive </a>
                </li>
              </ul>

            </div>
            <div className='col-md-12' style={{'border-bottom': '1px solid lightgray', 'padding-bottom': '10px'}}>
              <ul className='filterList' style={{'margin-top': '15px'}}>
                <li className='parent' style={{'margin-bottom': '7px'}}> Asset Class
                  <ul className='child'>
                    <li> <input type='checkbox' value='12' data-filter='Hello World'/> Hello World</li>
                    <li> <input type='checkbox' value='12' /> Hello World</li>
                    <li> <input type='checkbox' value='12' /> Hello World</li>
                    <li> <input type='checkbox' value='12' /> Hello World</li>
                  </ul>
                </li>
                <li className='parent' style={{'margin-bottom': '7px'}}> Fund Family
                  <ul className='child'>
                    <li> Demo Filter</li>
                    <li> Demo Filter</li>
                    <li> Demo Filter</li>
                    <li> Demo Filter</li>
                  </ul>
                </li>
                <li className='parent' style={{'margin-bottom': '7px'}}> Fund Type
                  <ul className='child'>
                    <li> Demo Filter</li>
                    <li> Demo Filter</li>
                    <li> Demo Filter</li>
                    <li> Demo Filter</li>
                  </ul>
                </li>
                <li className='parent' style={{'margin-bottom': '7px'}}> Fund Categories
                  <ul className='child'>
                    <li> Demo Filter</li>
                    <li> Demo Filter</li>
                    <li> Demo Filter</li>
                    <li> Demo Filter</li>
                  </ul>
                </li>
              </ul>
            </div>
            <div className='col-md-12' style={{'border-bottom': '1px solid lightgray', 'padding-bottom': '10px'}}>
              <div className='filter-panel-heading'>Performance Slider</div>
              <input id="performance" type="text" name="range_5" value="" />
            </div>
            <div className='col-md-12' style={{'border-bottom': '1px solid lightgray', 'padding-bottom': '10px'}}>
              <div className='filter-panel-heading'>AUM</div>
              <input id="aum" type="text" name="range_5" value="" />
            </div>
            <div className='col-md-12' style={{'border-bottom': '1px solid lightgray', 'padding-bottom': '10px'}}>
              <div className='filter-panel-heading'>Age Of Fund</div>
              <input id="fundage" type="text" name="range_5" value="" />
            </div>
          </div>
          <div className='col-md-9'>
            {filteredItemList}
          </div>
        </div>
        )
    }
  });

  return FundFilters;
});