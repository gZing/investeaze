define(["react", 'jsx!components/portfolio/portfolio', 'jsx!components/news/news', 'jsx!components/explore/explore'], function (React, Portfolio, News, Explore) {
  var Primary = React.createClass({
    getInitialState: function () {
      return {
        activeTab: 'Portfolio',
        activeSection: 'Notifications'
      }
    },
    componentDidMount: function () {
      this.setState({activeSection: this.props.activeSection});
    },
    render: function () {
      var height = $(window).height();
      var section;

      switch (this.props.activeTab) {
        case 'Portfolio':
          section = <Portfolio userId={this.props.userId} />
          break;
        case 'News':
          section = <News userId={this.props.userId} />
          break;
        case 'Explore':
          section = <Explore userId={this.props.userId} />
          break;
        default:
          section = <Portfolio userId={this.props.userId} />
          break;
      }

      return (
        <div style={{"height": 'auto'}}>
          {section}
        </div>
        );
    }
  });
  return Primary;
});