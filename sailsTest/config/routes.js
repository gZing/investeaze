/**
 * Routes
 *
 * Sails uses a number of different strategies to route requests.
 * Here they are top-to-bottom, in order of precedence.
 *
 * For more information on routes, check out:
 * http://sailsjs.org/#documentation
 */



/**
 * (1) Core middleware
 *
 * Middleware included with `app.use` is run first, before the router
 */


/**
 * (2) Static routes
 *
 * This object routes static URLs to handler functions--
 * In most cases, these functions are actions inside of your controllers.
 * For convenience, you can also connect routes directly to views or external URLs.
 *
 */
var passport = require("passport");

module.exports.routes = {

  // By default, your root route (aka home page) points to a view
  // located at `views/home/index.ejs`
  // 
  // (This would also work if you had a file at: `/views/home.ejs`)
  '/': {
    view: 'static/index'
  },

  // redirect view to close the social auth pop up windows
  '/redirect': 'views/redirect',
  // end

  // auth_user route to get current authenticated user
  'get /auth_user' : 'AuthController.getAuthUser',
  //end

  // =====================================
  // BASIC AUTH ROUTES =====================
  // =====================================

  'post /login': 'AuthController.login',
  'get /logout': 'AuthController.logout',

  // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  // END =====================
  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


  // =====================================
  // FACEBOOK ROUTES =====================
  // =====================================

  // route for facebook authentication and login
  'get /auth/facebook' : "AuthController.fbLogin", 
  // handle the callback after facebook has authenticated the user
  'get /auth/facebook/callback' : "AuthController.fbCallback",

  // facebook token auth route
  'post /auth/facebook/token' : "AuthController.fbToken",
  // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  // END =====================
  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


  // =====================================
  // LINKEDIN ROUTES =====================
  // =====================================

  // route for linkedin authentication and login
  'get /auth/linkedin' : "AuthController.linkedinLogin", 
  // handle the callback after facebook has authenticated the user
  'get /auth/linkedin/callback' : "AuthController.linkedinCallback",

  /// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  // END =====================
  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


  // =====================================
  // TWITTER ROUTES =====================
  // =====================================

  // route for twitter authentication and login
  'get /auth/twitter' : "AuthController.twitterLogin", 
  // handle the callback after twitter has authenticated the user
  'get /auth/twitter/callback' : "AuthController.twitterCallback",

  // twitter token auth route
  'post /auth/twitter/token' : "AuthController.twitterToken",
  //'post /auth/twitter/reverse' : "Authcontroller.twitterRev",

  /*
   * @author: Sambhav
   * Test Mappings
   */

  // User Controller

  'post /user' : "UserController.create",
  'get /user/:id' : "UserController.find",
  'get /user' : "UserController.list",
  'put /user/:id' : "UserController.update",
  'delete /user/:id' : "UserController.destroy",

  'get /user/followers/:id' : "UserController.fetchFollowers",
  'get /user/following/:id' : "UserController.fetchFollowing",
  'get /user/investment/:id' : "UserController.fetchInvestments",

  // Post Controller

  'post /post' : "PostController.create",
  'get /post/:id' : "PostController.find",
  'put /post/:id' : "PostController.update",
  'delete /post/:id' : "PostController.destroy",

  'get /post/list/:userId' : "PostController.fetchPosts",
  'get /idea/:userId' : "PostController.fetchIdeas",

  // Notification Controller

  'post /notification' : "NotificationController.create",
  'get /notification/find/:userId/:id' : "NotificationController.find",
  'get /notification/list/:userId' : "NotificationController.list",
  'delete /notification/:id' : "NotificationController.destroy",

  'get /notification/conn' : "NotificationController.connect",

  // News Controller

  'post /news' : "NewsController.create",
  'get /news/:id' : "NewsController.find",
  'get /news' : "NewsController.list",
  'put /news/:id' : "NewsController.update",
  'delete /news/:id' : "NewsController.destroy",


  // NewsFeed Controller

  'get /feed' : "NewsFeedController.list",

  // Message Controller

  'post /message' : "MessageController.create",
  'get /message/:id' : "MessageController.find",
  'delete /message/:id' : "MessageController.destroy",
  

  'get /message/interactions/:id' : "MessageController.getInteractions",
  'get /message/user/:userId' : "MessageController.getMessages",

  // FundUserMapping Controller

  // FundMetrics Controller

  // FundHouse Controller

  'post /fundhouse' : "FundHouseController.create",
  'get /fundhouse/:id' : "FundHouseController.find",
  'get /fundhouse' : "FundHouseController.list",
  'put /fundhouse/:id' : "FundHouseController.update",
  'delete /fundhouse/:id' : "FundHouseController.destroy",

  'get /fundhouse/followers/:id' : "FundHouseController.fetchFollowers",
  'get /fundhouse/backers/:id' : "FundHouseController.fetchBackers",

  // Fund Controller

  'get /fund/details/:fundId' : "FundController.getFundDetails",
  'get /fund/portfolio/:fundId' : "FundController.getFundPortfolioDetails",
  'get /fund/investment/:fundId' : "FundController.getFundInvestmentDetails",
  'get /fund/performance/:fundId' : "FundController.getFundPerformanceDetails",
  'get /fund/nav/:fundId' : "FundController.getFundNavs",
  'post /fund/list' : "FundController.getFundsList",
  'get /fund/filters' : "FundController.getFilters",

  /*'post /fund' : "FundController.create",
  'get /fund/:id' : "FundController.find",
  'get /fund' : "FundController.list",
  'put /fund/:id' : "FundController.update",
  'delete /fund/:id' : "FundController.destroy",

  'get /fund/followers/:id' : "FundController.fetchFollowers",
  'get /fund/backers/:id' : "FundController.fetchBackers",*/

  // Comment Controller

  'post /comment/' : "CommentController.create",
  'get /comment/:id' : "CommentController.find",
  'delete /comment/:id' : "CommentController.destroy",
  'put /comment/:id' : "CommentController.update",

  'get /comment/fetch/:postId' : "CommentController.fetchComments",

  // Auth Controller
  'post /auth/pusher' : "Authcontroller.pusherAuth",

  // Activity Controller

   'post /activity/' : "ActivityController.create",
   'get /like/:collection/:parentId' : "ActivityController.fetchLikes",
   'get /share/:parentId' : "ActivityController.fetchShares",
   'get /investment/:parentId' : "ActivityController.fetchInvestments",
   'get /refer/:collection/:parentId' : "ActivityController.fetchReferrals",

   'get /feed/:userId' : "NewsFeedController.find",

  /*
   * End Test Mappings 
   */

  /// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  // END =====================
  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


  // =====================================
  // UserController ROUTES ===============
  // =====================================

  'get /api/user/:id' : "UserController.getDetail",
  'put /api/user/:id' : "UserController.putDetail",

  'get /api/user' : "UserController.getList",
  'post /api/user': "UserController.postList",

  'get /follow/:id' : "UserController.addFollower",
  /// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  // END =====================
  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


  // =====================================
  // MessageController ROUTES =======
  // =====================================
  
 
  /// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  // END =====================
  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  //'get /user/:id' : "UserController.getUsers",
  /*
  // But what if you want your home page to display
  // a signup form located at `views/user/signup.ejs`?
  '/': {
    view: 'user/signup'
  }
  
  //notification routes

  // Let's say you're building an email client, like Gmail
  // You might want your home route to serve an interface using custom logic.
  // In this scenario, you have a custom controller `MessageController`
  // with an `inbox` action.
  '/': 'MessageController.inbox'


  // Alternatively, you can use the more verbose syntax:
  '/': {
    controller: 'MessageController',
    action: 'inbox'
  }


  // If you decided to call your action `index` instead of `inbox`,
  // since the `index` action is the default, you can shortcut even further to:
  '/': 'MessageController'


  // Up until now, we haven't specified a specific HTTP method/verb
  // The routes above will apply to ALL verbs!
  // If you want to set up a route only for one in particular
  // (GET, POST, PUT, DELETE, etc.), just specify the verb before the path.
  // For example, if you have a `UserController` with a `signup` action,
  // and somewhere else, you're serving a signup form looks like: 
  //
  //		<form action="/signup">
  //			<input name="username" type="text"/>
  //			<input name="password" type="password"/>
  //			<input type="submit"/>
  //		</form>

  // You would want to define the following route to handle your form:
  'post /signup': 'UserController.signup'


  // What about the ever-popular "vanity URLs" aka URL slugs?
  // (you might remember doing this with `mod_rewrite` in Apache)
  //
  // This is where you want to set up root-relative dynamic routes like:
  // http://yourwebsite.com/twinkletoez
  //
  // NOTE:
  // You'll still want to allow requests through to the static assets,
  // so we need to set up this route to ignore URLs that have a trailing ".":
  // (e.g. your javascript, CSS, and image files)
  'get /*(^.*)': 'UserController.profile'

  */
};



/** 
 * (3) Action blueprints
 * These routes can be disabled by setting (in `config/controllers.js`):
 * `module.exports.controllers.blueprints.actions = false`
 *
 * All of your controllers ' actions are automatically bound to a route.  For example:
 *   + If you have a controller, `FooController`:
 *     + its action `bar` is accessible at `/foo/bar`
 *     + its action `index` is accessible at `/foo/index`, and also `/foo`
 */


/**
 * (4) Shortcut CRUD blueprints
 *
 * These routes can be disabled by setting (in config/controllers.js)
 *			`module.exports.controllers.blueprints.shortcuts = false`
 *
 * If you have a model, `Foo`, and a controller, `FooController`,
 * you can access CRUD operations for that model at:
 *		/foo/find/:id?	->	search lampshades using specified criteria or with id=:id
 *
 *		/foo/create		->	create a lampshade using specified values
 *
 *		/foo/update/:id	->	update the lampshade with id=:id
 *
 *		/foo/destroy/:id	->	delete lampshade with id=:id
 *
 */

/**
 * (5) REST blueprints
 *
 * These routes can be disabled by setting (in config/controllers.js)
 *		`module.exports.controllers.blueprints.rest = false`
 *
 * If you have a model, `Foo`, and a controller, `FooController`,
 * you can access CRUD operations for that model at:
 *
 *		get /foo/:id?	->	search lampshades using specified criteria or with id=:id
 *
 *		post /foo		-> create a lampshade using specified values
 *
 *		put /foo/:id	->	update the lampshade with id=:id
 *
 *		delete /foo/:id	->	delete lampshade with id=:id
 *
 */

/**
 * (6) Static assets
 *
 * Flat files in your `assets` directory- (these are sometimes referred to as 'public')
 * If you have an image file at `/assets/images/foo.jpg`, it will be made available
 * automatically via the route:  `/images/foo.jpg`
 *
 */



/**
 * (7) 404 (not found) handler
 *
 * Finally, if nothing else matched, the default 404 handler is triggered.
 * See `config/404.js` to adjust your app's 404 logic.
 */
 
