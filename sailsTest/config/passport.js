var passport    = require('passport'),
  LocalStrategy = require('passport-local').Strategy,
  LocalAPIKeyStrategy = require('passport-localapikey').Strategy,
  FacebookStrategy = require('passport-facebook').Strategy,
  LinkedInStrategy = require('passport-linkedin').Strategy,
  TwitterStrategy = require('passport-twitter').Strategy,
  FacebookTokenStrategy = require('passport-facebook-token').Strategy,
  TwitterTokenStrategy = require('passport-twitter-token').Strategy,
  bcrypt = require('bcrypt-nodejs'),
  session = require('express-session'),
  configAuth = require('../config/auth');


// helper functions

function findById(id, fn) {
  User.findOne(id).done(function (err, user) {
    if (err) {
      return fn(null, null);
    } else {
      return fn(null, user);
    }
  });
}

// end here


passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function (err, user) {
    done(err, user);
  });
});

/*
passport.use(new LocalAPIKeyStrategy(

  function(apikey, done) {

    User.findOne({ api_key: apikey }, function (err, user) {
      if (err) { return done(err); }
      if (!user) { return done(null, false); }
      return done(null, user);
    });
  }
));
*/

// =================== Basic Auth strategy ======================

// Using LocalStrategy for Basic Auth
passport.use(new LocalStrategy(
  function(username, password, done) {
    User.findOne({username: username}, function(err, user) {

      if (err) { 
        return done(null, err); 
      }
      // user not found
      if (!user || user.length < 1) { 
        return done(null, false, { message: 'Incorrect User'}); 
      }

      // comparing the password hash with the hash in the database
      bcrypt.compare(password, user.password, function(err, res) {
        // password hash match failure
        if (!res){
          return done(null, false, { message: 'Invalid Password'});
        }

        //password hash match successful
        return done(null, user);

      });

    });

  }) 

);

// =================== <<< END >>> ======================

// =================== Facebook Auth strategy ======================

passport.use(new FacebookStrategy({
    clientID: configAuth.facebookAuth.clientID,
    clientSecret: configAuth.facebookAuth.clientSecret,
    callbackURL: configAuth.facebookAuth.callbackURL,
    enableProof: false
  }, function (accessToken, refreshToken, profile, done) {

    // find user if alread exists
    User.findOne({
     where: {
      or: [
       {facebookId : profile.id} ,
       { email : profile.emails[0].value } 
      ]
     }
    }, function (err, user) {

      // Create a new User if it doesn't exist yet
      if (!user) {

        User.create({

          name : profile.displayName ,
          imageLink : 'https://graph.facebook.com/' + profile.id + '/picture?type=large',
          email : profile.emails[0].value,
          facebookId : profile.id,
          profile_facebook : profile.profileUrl
        // You can also add any other data you are getting back from Facebook here 
        // which is supported by your Model.

        }).done(function (err, user) {
          if (user) {
            // User created and invoking done() to authenticate user
            return done(null, user, {
              message: 'Logged In Successfully'
            });
          } else {
            // User creation failed and cannot be logged in
            return done(err, null, {
              message: 'There was an error logging you in with Facebook'
            });
          }
        });

      // If there is already a user, return it
      } else {
        return done(null, user, {
          message: 'Logged In Successfully'
        });
      }
    });
  }
));

// =================== <<< END >>> ======================


// =================== LinkedIn Auth strategy ======================

passport.use(new LinkedInStrategy({
    consumerKey: configAuth.linkedInAuth.consumerKey,
    consumerSecret: configAuth.linkedInAuth.consumerSecret,
    callbackURL: configAuth.linkedInAuth.callbackURL
  },
  function(token, tokenSecret, profile, done) {

    // find user if alread exists
    User.findOne({
     where: {
      or: [
       {linkedInId : profile.id },
       // data returned contains an array of emails, checking with the zero index
       {email : profile.emails[0].value} 
      ]
     }
    }, function (err, user) {

      // Create a new User if it doesn't exist yet
      if (!user) {
        User.create({

          name : profile.displayName ,
          imageLink : profile.photos,
          // data returned contains an array of emails, saving value at zero index
          email : profile.emails[0].value,
          linkedInId : profile.id,
          profile_linkedIn : profile.url
          // You can also add any other data you are getting back from Facebook here 
          // as long as it is in your model

        }).done(function (err, user) {

          if (user) {
            // User created and invoking done() to authenticate user
            return done(null, user, {
              message: 'Logged In Successfully'
            });
          } else {
            // User creation failed and cannot be logged in
            console.log(err);
            return done(err, null, {
              message: 'There was an error logging you in with LinkedIn'
            });
          }
        });

      // If there is already a user, return it
      } else {
        return done(null, user, {
          
          message: 'Logged In Successfully'
        });
      }
    });
  }
));

// =================== <<< END >>> ======================

// =================== Twitter Auth strategy ======================

passport.use(new TwitterStrategy({
    consumerKey: configAuth.twitterAuth.consumerKey,
    consumerSecret: configAuth.twitterAuth.consumerSecret,
    callbackURL: configAuth.twitterAuth.callbackURL
  },

  function(token, tokenSecret, profile, done) {
    
    // twitter gives 'normal' thumbnail image by default
    // converting the URL to request the API with a
    // image of 400x400 resolution

    var index = profile.photos[0].value.indexOf('normal');

    // gets the format of the image
    var format = profile.photos[0].value.slice(index + 6)
    
    //appends resolution and format of the image
    var user_pic = profile.photos[0].value.slice(0,index).concat('400x400' + format);

    // find user if alread exists 
    User.findOne({twitterId : profile.id} , function (err, user) {

      // Create a new User if it doesn't exist yet
      if (!user) {
        User.create({

          name : profile.displayName ,
          imageLink : user_pic,
          twitterId : profile.id,
          profile_twitter : "http://twitter.com/" + profile.username
  
        }).done(function (err, user) {

          if (user) {
            // User created and invoking done() to authenticate user
            return done(null, user, {
              message: 'Logged In Successfully'
            });
          } else {
            // User creation failed and cannot be logged in
            return done(err, null, {
              message: 'There was an error logging you in with Twitter'
            });
          }
        });

      // If there is already a user, return it
      } else {

        return done(null, user, {
          
          message: 'Logged In Successfully'
        });
      }
    });
  }

));

// =================== <<< END >>> ======================

// =================== Facebook Token Auth strategy ======================

passport.use(new FacebookTokenStrategy({
    clientID: configAuth.facebookAuth.clientID,
    clientSecret: configAuth.facebookAuth.clientSecret
    }, function (accessToken, refreshToken, profile, done) {
    User.findOne({
     where: {
      or: [
       { facebookId : profile.id} ,
       { email : profile.emails[0].value } 
      ]
     }
    }, function (err, user) {
      // Create a new User if it doesn't exist yet
      if (!user) {

        console.log('Yup we are in');
        User.create({

          name : profile.displayName ,
          imageLink : 'https://graph.facebook.com/' + profile.id + '/picture?type=large',
          email : profile.emails[0].value,
          facebookId : profile.id
        // You can also add any other data you are getting back from Facebook here 
        // which is supported by your Model.

        }).done(function (err, user) {

          if (user) {
            console.log('User already exists');

            return done(null, user);
          } else {
            console.log(err);
            return done(err, null, {
              message: 'There was an error logging you in with Facebook'
            });
          }
        });

      // If there is already a user, return it
      } else {
        return done(null, user, {
          
          message: 'Logged In Successfully'
        });
      }
    });
  }
));

// =================== <<< END >>> ======================

// =================== Twitter Auth strategy ======================

passport.use(new TwitterTokenStrategy({
    consumerKey: configAuth.twitterAuth.consumerKey,
    consumerSecret: configAuth.twitterAuth.consumerSecret,
  },

  function(token, tokenSecret, profile, done) {
    var index = profile.photos[0].value.indexOf('normal');
    var format = profile.photos[0].value.slice(index + 6)
    
    var user_pic = profile.photos[0].value.slice(0,index).concat('400x400' + format);

    User.findOne({twitterId : profile.id} , function (err, user) {

      // Create a new User if it doesn't exist yet
      if (!user) {
        User.create({

          name : profile.displayName ,
          imageLink : user_pic,
          twitterId : profile.id,
          profile_twitter : "http://twitter.com/" + profile.username,
          password : 'defaultPassword'

          // You can also add any other data you are getting back from Facebook here 
          // as long as it is in your model

        }).done(function (err, user) {

          if (user) {
            console.log("User Created ");
            return done(null, user, {
              message: 'Logged In Successfully'
            });
          } else {
            console.log("User Not Created  .. DB error supposedly");
            return done(err, null, {
              message: 'There was an error logging you in with Twitter'
            });
          }
        });

      // If there is already a user, return it
      } else {

        return done(null, user, {
          
          message: 'Logged In Successfully'
        });
      }
    });
  }

));

// =================== <<< END >>> ======================

module.exports = {
 express: {
    customMiddleware: function(app){
      app.use(passport.initialize());
      app.use(passport.session());
    }
  }
};