module.exports = {
    
    attributes: {

        // Id of the fund in User Model
        fundId : {
            type: "string",
            required: true
        },
        nav : {
            type: "float",
            required: true
        },
        date : {
            type: "date",
            required: true

        }
    }
};