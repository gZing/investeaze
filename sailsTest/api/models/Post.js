
module.exports = {

    attributes: {

        // Initiator is the person who is the creater/owner of the post
        initiator : {
            type : 'string',
            required : true
        },

        postType : {
            type : 'string' // post/idea 
        },

        content : {
            type : 'text',
            required : true
        },

        numberOfLikes : {
            type : 'integer',
            defaultsTo : 0
        },

        numberOfComments : {
            type : 'integer',
            defaultsTo : 0
        },

        numberOfShares : {
            type : 'integer',
            defaultsTo : 0
        },

        // If I share the post by user AAA then there will
        // be a new entry where I will be the initiator and
        // sharedFrom will have the reference to AAA
        sharedPostId : {
            type : 'string'
        },

        // incase I write the post on someone else's wall
        // postDestinationOwner will have the id of the person
        // on whose wall I have written the post
        postDestinationOwner : {
            type : 'string'
        }
    }
};

/*
*  Post P of user A is  shared by user B which generates a new post O,
*   P : parentId
*   A : parentIdInitiator
*   B : initiator
*   O : sharedPostId 
*/
