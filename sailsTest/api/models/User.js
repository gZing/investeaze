/**
 * User
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */
var bcrypt = require('bcrypt-nodejs');

module.exports = {

  attributes: {

    /*
    * Need to store user investments
    */
    
    // ------- authkeys -------
    twitterId  : {
      type : 'string'
    },
    facebookId : {
      type : 'string'
    },
    linkedInId : { 
      type :'string'
    },
    // -------

    // ------- personal details ------

    name : {
      type : 'string'
    },

    dob : {
      type : 'date'
    },

    location : {
      type : 'string'
    },

    username : {
      type : 'string',
      regex : /^[a-z0-9_-]{3,15}$/
    },

    type: {
      type: "string",
      required: true,
      defaultsTo : "User",
      in: ["User","Fund"]
    },

    email : {
      type : 'email'
    },

    password : {
      type : 'string'
    },

    address: {
      type : 'string'
    },

    city : {
      type : 'string'
    },

    state : {
      type : 'string'       
     },

    country : {
      type : 'string'
    },

    imageLink : {
      type : 'string'
    }, 
  
    // Social Netowrk Profile Links
    profileFacebook : 'string',
    profileTwitter  : 'string',
    profileLinkedIn : 'string',

    accredited : {
      type : 'boolean'
    },

    /* investments: 
        [
          {amount:123.32, fundId: "someId", date: "Linux Date"},
          {amount:223.32, fundId: "someOtherId", date: "Linux Date 2"}
          , ...
        ] 
    */

    investments: {                    
      type : 'array',
      defaultsTo : []
    },

    /* badges: 
        [
          {id, text},
          {id, text}
        ] 
    */

    badges: {                    
      type : 'array',
      defaultsTo : []
    },

    
    /*  tags:
          [
            {id, text},
            {id, text}
          ] 
    */

    tags : {
      type: 'array',
      defaultsTo : []
    },

    emailActivated: {
      type : 'boolean'
    },

    lastLoggedIn: {
      type : 'datetime'
    },

    userHash : {
      type : 'string'
    },

    profileTitle:{
      type : 'array'
    },

    /*  previousCompanies
        [
          {"name":"Goldmann Sachs"}
        ]
    */

    previousCompanies: {
      type : 'array',
      defaultsTo : []
    },

    /*  follower:
          [
            {id}
          ] 
    */
      
    followers : {
      type : 'array',
      defaultsTo : []
    },

    /*  following:
          [
            {
              id: entityId --> entityId = Id of User/Fund/FundHouse
              collection: collectionName --> User/Fund/FundHouse
            }  
          ] 
    */
    following : {
      type : 'array',
      defaultsTo : []
    },

    isDeleted : {
      type : 'boolean',
      defaultsTo : false
    },
    
    /*
    * This method converts a User object to JSON format
    */
    toJSON: function() {
      var obj = this.toObject();
      var user = User.find({username:'gagan'},{ username:1 }, function(err, user) {

        if(err){
            console.log(err)
        }
        else{
            console.log(user)
        }

        });
      delete obj.password;
      return obj;
    }
  },

  /*
  * This method is exectued before the User object is created
  */
  beforeCreate: function(user, cb) {

    // The following code makes a hash of the password using the salt
    bcrypt.genSalt(10, function(err, salt) {

        bcrypt.hash(user.password,salt, null, function(err, hash) {
            if (err) {
                console.log(err);
                cb(err);
            }else{
                user.password = hash;
            }
        });

        bcrypt.hash(user.api_key,salt, null, function(err, hash) {
            if (err) {
                console.log(err);
                cb(err);
            }else{
                user.api_key = hash;
                cb(null, user);
            }
        });
    });
  }
}
