/**
 * Message
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {
  	from : {
        type: "string",
        required: true
    }, 

    // If the sender has archived the message
    archived : {
        type: 'boolean',
        required : true,
        defaultsTo: false
    },


    to : {
        type : 'string',
        required : true
    },

    // If the receiver has read the message
    receiverRead : {
        type: 'boolean',
        required: true,
        defaultsTo: false
    },

    // If the reciver has archived the message
    receiverArchived : {
        type: 'boolean',
        required: true,
        defaultsTo: false
    },


    content : {
        type : 'text',
        required : true
    },

    // array of links to attachments ["http://some attachment.jpg","http://some attachment.pdf", ..]
    attachment : {
        type : 'array',
        defaultsTo: []
    }
  }

};
