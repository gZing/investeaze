module.exports = {
    
    attributes: {

        // Id of the fund in User Model
        fundId : {
            type: "string",
            required: true
        },
        nav : {
            type: "float",
            required: true
        },
        navChange : {
            type: "float"
        },
        navChangePercentage : {
            type: "float",
        },
        date : {
            type: "date",
            required: true
        },

        /*
        [
            {
                "period" : "1 month",
                "returns" : 11.3
                "rank" : 24
            },
            {
                "period" : "3 month",
                "returns" : 23.3
                "rank" : 51
            }, .....
        ]
        */
        returns : {
            type : "array",
            defaultsTo: []
        },
        
        /*
        [
            {
                "year" : 2014,
                "qtr1" : 6.9
            },
            {
                "year" : 2013,
                "qtr1" : 5.4,
                "qtr2" : 3.9,
                "qtr3" : 1.6,
                "qtr4" : 2.7,
                "annual" : 5.4,
            }, .....
        ]
        */
        absoluteReturns : {
            type : "array",
            defaultsTo: []
        }
    }
};