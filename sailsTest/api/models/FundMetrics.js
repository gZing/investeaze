/**
 * FundMetrics
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {

  	fundId : {
        type: "string",
        required: true
    },
    priceDate : {
    	type: "date" 
    },
    price : {
        type: "float"
    },
    sharpRatioDate : {
    	type: "date" 
    },
    sharpRatioPrice : {
        type: "float"
    },
    yearlyAlphaDate : {
    	type: "date" 
    },
    yearlyAlphaPrice : {
        type: "float"
    },
    threeMonthsReturnDate : {
    	type: "date" 
    },
    threeMonthsReturnPrice : {
        type: "float"
    },
    sixMonthsReturnDate : {
    	type: "date" 
    },
    sixMonthsReturnPrice : {
        type: "float"
    }
  }

};