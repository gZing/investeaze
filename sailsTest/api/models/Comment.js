
module.exports = {

    attributes: {

        // The user who commented
        initiator : {
            type : 'string',
            required : true
        },

        // Id of the Post on which the comment was made
        parentId : {
        	type : 'string',
        	required : true
        },

        content : {
            type : 'string',
            required: true            
        },

        // id of the person who initiated the parent post
        // helps in notifications
        parentIdInitiator : {
        	type : 'string',
        	required : true
        },

        numberOfLikes : {
            type : 'integer',
            defaultsTo : 0
        }

    }

};