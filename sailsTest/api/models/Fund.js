/**
 * Fund
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

module.exports = {
    
    attributes: {

        // Id of the fund in User Model
        fundId : {
            type: "string",
            required: true
        },
        fundName : {
            type: "string",
            reqired: true
        },
        fundFamily : {
            type: "string"
        },
        fundClass : {
            type: "string"
        },
        fundCategory : {
            type: "string"
        },
        aum : {
            type: "float",
            required: true
        },
        type : {
            type: "string"
        },
        returns : {
            type : "float"
        },
        currentCrisilRank : {
            type: "integer",
            max: 5,
            min: 1
        },
        previousCrisilRank : {
            type: "integer",
            max: 5,
            min: 1
        },
        // Date of the Fund to calculate age
        date : {
            type: "date",
            required: true
        },
    }
};

/*module.exports = {

  attributes: {

  	fundHouseId : {
        type: "string",
        required: true
    },
    
    description : {
    	type: "text"
    },
    
    // array of string values ["value1","value2", ..]
    strategy : {
    	type: "array"
    },
    
    name : {
    	type: "string"
    },
    
    founded : {
    	type: "date"
    },
    
    location : {
    	type: "string"
    },
    
    email: {
    	type: 'email',
	    required: true
    },
    
    address_name : {
    	type: "string"
    },
    
    street : {
    	type: "string"
    },
    
    city : {
    	type: "string"
    },
    
    state : {
    	type: "string"
    },

    country : {
    	type: "string"
    },

    phone : {
    	type: "string"
    },
    
    logoImage : {
    	type: "string"
    },
    
    //This is an array of Sub-UserProfile objects: 
    //[{userId:"122233",(UserProfile Values),(Usersocial rpofile links)}, {...]
    team : {
    	type: "array",
      defaultsTo : []
    },
    
    // array for tagId, and Tag Text [{tagId:"2w21w", tagText:"TagValue"},{..]
    tags : {
    	type: "array",
      defaultsTo : []
    },

    // array for Questions and answers [{qText:"What is a Fund House?", aText:"TIt is a house of funds"},{..]
    qna : {
    	type: "array",
      defaultsTo : []
    },
    
    // array of links to pitch documents ["http://wtf.com/hell/yeah", ..]
    pitch : {
    	type: "array"
    },

    // array of user Id's who follow the fund ["id1", "id2", ..]
    followers : {
    	type: "array",
      defaultsTo : []
    },
    
    startDate : {
    	type: "date" 
    },

    endDate : {
    	type: "date"
    },

    // array of JSON documents with Id's of users who invested, the amount which
    // which they invested and the date of investment. [{id:"someID",amount:12.2, date:LinuxTime},{...]
    backers : {
        type: "array" 
    },
    amountRaising : {
    	type: "float"
    },

    raisedTillNow : {
    	type: "float",
        defaultsTo : 0.0
    },

    // An array of objects with initiator and initiated
    // [{initiator: "userId", initiated:"userId"}, {...]
    referrals : {
    	type: "array",
        defaultsTo : []
    },

    // array of strings ["term1", "term2", ..]
    terms : {
    	type: "array"
    },

    numberOfLikes : {
        type : 'integer',
        defaultsTo : 0
    },
    isDeleted : {
        type: 'boolean',
        defaultsTo: false
    }
  }
 };*/