/**
 * News
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {
  	link : {
    	type: "string",
    	required: true
    },
    articleType : {
    	type: "string" // Either of the following three: news/newsletter/filing
    },
  	date : {
    	type: "date"
    },
    entityCollection : {
    	type: "string", // Either of the following two: fund/fundHouse
      required : true
    },
    entityId : {
        type: "string",
        required: true
    }
  }
};