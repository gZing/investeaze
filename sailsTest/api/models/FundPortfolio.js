module.exports = {
    
    attributes: {

        // Id of the fund in User Model
        fundId : {
            type: "string",
            required: true
        },

        // Holdings

        /*
        [
            {
                "equity": "HDFC Bank",
                "sector": "Banking/Finance",
                "value": 54.83,
                "assets": 9.57
            },
            {
                "equity": "Power Grid Corp",
                "sector": "Utilities",
                "value": 51.22,
                "assets": 8.94
            },.....
        ]
        */
        holdings : {
            type: "array",
            defaultsTo: []
        },

        // Asset Allocation

        /*
        [
            {
                "class" : "Equity",
                "value" : 94.45
            },
            {
                "class" : "Others / Unlisted",
                "value" : 0.29
            },...
        ]
        */
        assetAllocation : {
            type: "array",
            defaultsTo: []
        },
        

        // Sector Allocation

        /*
        [
            {
                "sector" : "Banking/Finance",
                "value" : 28.74,
                "high" : 28.74,
                "low" : 18.38

            },
            {
                "class" : "Technology",
                "value" : 15.92,
                "high" : 15.92,
                "low " : 8.52
            },...
        ]
        */
        sectorAllocation : {
            type: "array",
            defaultsTo: []
        },
        

        // Concentration


        /*
        [
            {
                "type" : "holding",
                "attribute" : "Top 5",
                "value" : 28.74,
                "low" : 37.38

            },
            {
                "type" : "holding",
                "attribute" : "Top 10",
                "value" : 53.92
            },
            {
                "type" : "sector",
                "attribute" : "Top 3",
                "value" : 55.37
            },
        ]
        */
        concentration : {
            type: "array",
            defaultsTo: []
        }
        
    }
};