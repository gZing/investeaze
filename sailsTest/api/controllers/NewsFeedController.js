/**
 * NewsFeedController
 */

// Imports 
var async = require("async");
var ObjectId = require('mongodb').ObjectID
var Pusher = require('pusher');

// Initializing
var pusher = new Pusher({
  appId: '77682',
  key: '1d21f8b62586ce0ac86f',
  secret: '83214175fc4c5379c17f'
});

module.exports = {
    
  list : function(req, res){

  	var request = req.body;
  	var userId  = req.session.passport.user;

  	var id;
  	if(request.id)
  		id = request.id;
  	else
  		id = "ffffffffffffffffffffffff"; //maximum possible id in the ObjectId
  	
  	var maxFeed = 20; //maximum feeds to fetch

  	var data = [];

  	User.findOne(
  		{ 
  			_id : new ObjectId(userId) 
  		}
  	).done(
  		function(err, user){
        console.log("User Found", user);
  		if(!user){
        return res.json({error: 'User Not Found'});
      }
  		var followers = user.followers;
  		
  		if(followers){
  			
	  			// Query for all the posts / comments / activities which are less
	  			// than a particular id and have one of the followers as the initiator
	  			// All the queries are done in parallel.
			  	async.parallel(
			  		[
						function(callback) {
				  			Post.native(
				  				function(err,collection) {
					  				collection.find(
					  					{ 
					  						_id : { 
					  							"$lt" : new ObjectId(id)
					  						},
					  						initiator : { 
					  							"$in" : followers 
					  						}	 
					  					}
					  				)
					  				.sort( { 
					  						_id : -1
					  					} 
					  				)
					  				.limit(maxFeed)
					  				.toArray(
					  					function(err, posts) {
					  						if(err)
					  							callback(err, []);			  				
					  						console.log("Post callback ", posts.length);
					  						callback(null, posts);
					  					}
					  				);
				  				}
				  			);
				  		},

				  	
					  	function(callback){
					  		Comment.native(
					  			function(err,collection) {
					  				collection.find(
					  					{ 
					  						_id : { 
					  							"$lt" : ObjectId(id)
					  						},
					  						initiator : { 
					  							"$in" : followers 
					  						} 
					  					}
					  				)
					  				.sort( {
					  						_id : -1
					  					} 
					  				)
					  				.limit(maxFeed)
					  				.toArray(
					  					function(err, comments) {
					  						if(err)
					  							callback(err, []);
					  						console.log("Comment callback ", comments.length);
					  						callback(null, comments);
					  					}
					  				);
					  			}
					  		);
					  	},

				  	
					  	function(callback){
					  		Activity.native(
					  			function(err,collection) {
					  				collection.find(
					  					{ 
					  						_id : {
					  							"$lt" : ObjectId(id)
					  						},
					  						initiator : {
					  							"$in" : followers 
					  						}
					  					}
					  				)
					  				.sort( {
					  						_id : -1
					  					} 
					  				)
					  				.limit(maxFeed)
					  				.toArray(
					  					function(err, activities) {
							  				if(err)
							  					callback(err, []);
							  				console.log("Activity callback ", activities.length);
							  				callback(null, activities);
					  					}
					  				);
					  			}
					  		);
					  	}
					],

				  	function(err, inputs){
				  		if (err)
				  			return res.json(err);
				  		// Adding the feeds to a single results list
				  		results = inputs[0];
				  		results.push.apply(results, inputs[1]);
				  		results.push.apply(results, inputs[2]);

				  		// Sorting the entitiesFollowed list on basis of id
				  		results.sortById = function() {
						    this.sort(function(a,b) {
						    	if (b._id > a._id)
						    		return 1;
						    	else return -1;
						    });
						};
						  results.sortById();
				  		console.log("Final Callback " + results.length);
				  		return res.json(results.slice(0, maxFeed));
				  	}

				);
	  		}
	  	}
	  );
  },

  // Create a news feed
  create : function(req, res){

	User.findOne({
			_id : req.body.initiator
		}
	).done(
		function(err, user) {

			// Error handling
			if (err) {
				// The Post could not be created!
				console.log("Error Finding User: " + JSON.stringify(err));
				return res.json(
					{
						success: false,
						message: "Error Finding User"
					}
				);
			} else {
				console.log("Publishing News Feed")
				var followers = user.followers;

				/*
				* Iterating over the followers of the feed initiator,
				* if a follower is online, then the feed is sent to their feeds socket.SS
				*/
				/*for(var i=0; i < followers.length; i++){
					if(sails.config.globals.onlineFeeds[followers[i]]) {
		                console.log("User " + followers[i] + " is currently logged in. Sending Notification message..");
		                sails.io.sockets.socket(sails.config.globals.onlineFeeds[followers[i]]).emit('notificationReceived', req.body);
		            }
				}*/

				for(var i=0; i < followers.length; i++){

					//=== Pusher code

			 		/*
			 		* Sending feed to the user if the user is online, i.e.
			 		* its feed channel is active. We assume that the feed 
			 		* channel for each user is of the form:
			 		* presence-feedChannel-userId
			 		*/

			 		var channelName = 'presence-feedChannel-' + followers[i];

			 		/*
			 		* Check for the member count of the user's feed channel to 
			 		* check whether the user is online or not.
			 		*/

			 		pusher.get( { path: '/channels/'+channelName+'/users', params: {} },
					  	function( error, request, response ) {
					    	if( response && response.statusCode === 200 ) {
					      		
					      		if(JSON.parse(response.body).users.length > 0){

					      			/*
					      			* User is online, sending feed to user.
					      			* We send the feed at the event called ,
					      			* newFeed, and we assume that the user's
					      			* feedChannel will be binded to this event
									*/
					      			console.log('sending feed to user: ' + followers[i]);

			 						pusher.trigger(
			 							channelName,
			 							'newFeed', 
			 							req.body
			 						);			
			 					}
					    	}
					  	}
					);
				}
			}
		}
	);
  },

  connect: function(req, res){

        // Fetching socket from the request
        var socket = req.socket;

        // UserId which is sent from the client side
        var userId = req.param('user');

        console.log('Feeds Socket Connected for user: '+userId);

        // setting username for the socket
        socket.username = userId;

        /*
        * Check if the user is already active, i.e if the user already exists in the 
        * onlineFeeds list. onlineFeeds list keeps the active userId as the key and 
        * socketId as the value corresponding to it. This helps us to find out if the 
        * recepient of a message in currently online or not, so that we can emit message to
        * corresponding socket for the recepient.
        */
        if(sails.config.globals.onlineFeeds[userId])
                console.log("User already active");
        else {
            sails.config.globals.onlineFeeds[userId] = socket.id;
            console.log("User now active: "+userId);
        }

        socket.on('disconnect',
            function(){
                // Socket is disconnected, we need to delete the userId from global onlineFeeds list.
                delete sails.config.globals.onlineFeeds[socket.username];
                console.log("User disconnected: "+socket.username);
            }
        );

        res.json({success : true});
    }

  
};
