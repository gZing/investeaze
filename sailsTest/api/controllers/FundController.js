var FundHouseController  = require('./FundHouseController');
var ObjectId = require('mongodb').ObjectID;

module.exports = {

  getFundsList: function(req, res){

    var filters = req.body;

    console.log("Fetching funds list with filters: " + JSON.stringify(filters));

    var fundJsonQuery = {};
    var fundPerformanceJsonQuery;
    var fundDetailsJsonQuery = {};

    /*
     fundPerformanceJsonQuery = { "returns" : {
                                    $elemMatch : {
                                      "entity" : "1 year"
                                    }
                                  }
                                };
      fundPerformanceJsonQuery.returns.$elemMatch.value = performanceFilter;
    */

    if(filters.performance) {

      var performanceFilter = {};

      if(filters.performance.gt){
        performanceFilter.$gt = filters.performance.gt;
      }

      if(filters.performance.lt){
        performanceFilter.$lt = filters.performance.lt;
      }
 
      fundJsonQuery.returns = performanceFilter;
    }

    if(filters.age) {

      var ageFilter = {};

      if(filters.age.gt){
        var start = (new Date).getTime() - filters.age.gt;
        ageFilter.$gt = start;
      }

      if(filters.age.lt){ 
        var end = (new Date).getTime() - filters.age.lt;
        ageFilter.$lt = end;
      }

      fundJsonQuery.date = ageFilter;

    }

    if(filters.aum) {
      var aumFilter = {};

      if(filters.aum.gt){
        aumFilter.$gt = filters.aum.gt;
      }

      if(filters.aum.lt){
        aumFilter.$lt = filters.aum.lt;
      }

      fundJsonQuery.aum = aumFilter;

    }

    if(filters.assetClass) {
      fundJsonQuery.assetClass = filters.assetClass;
    }

    if(filters.fundFamily) {
      fundJsonQuery.fundFamily = filters.fundFamily;
    }

    if(filters.fundType) {
      fundJsonQuery.fundType = filters.fundType;
    }

    if(filters.fundCategory) {
      fundJsonQuery.fundCategory = filters.fundCategory;
    }

    Fund.native(
      function(err, collection) {
        if(err){
          console.log("Error creating native query for Funds");
          res.json("Error Fetching Funds List",400);
        } else {
          collection.find(
            fundJsonQuery
          )
          .limit(10)
          .toArray(
            function(err, funds) {
              if(err) {
                console.log("Error Finding Funds");
                res.json("Error Fetching Funds List", 400);
              } else {
                console.log("Success Fetching Funds List");
                res.json(
                  funds,
                  200
                );
              }
            }
          )
        }
      }
    )
  },

  getFundDetails: function(req, res){
    
    var fundId = req.param('fundId');
    console.log('Fetching Fund Details for FundId: '+fundId);

    Fund.native(
      function(err, collection){
        if(err){
          
          console.log("Error creating native query for Fund");
          return res.json(
                  "Error Fetching Fund Details",
                  400
                );

        } else {
          collection.findOne(
            {
              fundId: fundId
            },
            function(err, fundDetails){
              if(err){
              
                console.log("Error Finding Fund!");
                return res.json(
                  "Error Fetching Fund Details",
                  400
                );
              
              } else {

                return res.json(
                  fundDetails,
                  200
                ); 

              }
            }
          );
        }
      }
    );
  },

  getFilters: function(req, res) {
    console.log("Fetching Filters");

    var fetchFiltersQuery = 
      {
        $group : 
        {
          _id  : "",
          maxAum : 
          {
            $max : "$aum"
          },
          minAum :
          {
              $min: "$aum"
          },
          maxPerformance : {
            $max : "$returns"
          },
          minPerformance : {
              $min : "$returns"
          },
          maxDate : {
              $max : "$date"
          },
          minDate : {
              $min : "$date"
          }
        }
      };

    Fund.native(
      function(err, collection) {
        if(err) {
          console.log("Could not create native query for Fund");
          res.json("Error Fetching Fund Filters" , 400);
        } else {
          collection.aggregate(
            fetchFiltersQuery,
            function(err, result) {
              if(err) {
                
                console.log("Could not fetch filters");
                res.json("Error Fetching Fund Filters" , 400);

              } else {
                console.log("Success in fetching filters");

                var currentYear = new Date().getFullYear()
                var maxYear = new Date(result[0].maxDate).getFullYear();
                var minYear = new Date(result[0].minDate).getFullYear();

                var maxAge = currentYear - minYear;
                var minAge = currentYear - maxYear;

                var filters = {
                  maxAge: maxAge,
                  minAge: minAge,
                  maxAum: result[0].maxAum,
                  minAum: result[0].minAum,
                  maxPerformance: result[0].maxPerformance,
                  minPerformance: result[0].minPerformance
                };

                res.json(filters, 200);
              }
            }
          )
        }
      }
    )
  },

  getFundPortfolioDetails: function(){
      var fundId = req.param('fundId');
      console.log('Fetching FundPortfolio Details for FundId: '+fundId);

      FundPortfolio.native(
        function(err, collection){
          if(err){
            
            console.log("Error creating native query for FundPortfolio");
            return res.json(
                    "Error Fetching FundPortfolio Details",
                    400
                  );

          } else {
            collection.findOne(
              {
                fundId: fundId
              },
              function(err, fundPortfolioDetails){
                if(err){
                
                  console.log("Error Finding FundPortfolio!");
                  return res.json(
                    "Error Fetching FundPortfolio Details",
                    400
                  );
                
                } else {

                  return res.json(
                    fundPortfolioDetails,
                    200
                  ); 

                }
              }
            );
          }
        }
      );
  },

  getFundInvestmentDetails: function(){
    var fundId = req.param('fundId');
      console.log('Fetching FundInvestment Details for FundId: '+fundId);

      FundInvestment.native(
        function(err, collection){
          if(err){
            
            console.log("Error creating native query for FundInvestment");
            return res.json(
                    "Error Fetching FundInvestment Details",
                    400
                  );

          } else {
            collection.findOne(
              {
                fundId: fundId
              },
              function(err, fundInvestmentDetails){
                if(err){
                
                  console.log("Error Finding FundInvestment!");
                  return res.json(
                    "Error Fetching FundInvestment Details",
                    400
                  );
                
                } else {

                  return res.json(
                    fundInvestmentDetails,
                    200
                  ); 

                }
              }
            );
          }
        }
      );
  },

  getFundPerformanceDetails: function(){
    var fundId = req.param('fundId');
    console.log('Fetching FundPerformance Details for FundId: '+fundId);

    FundPerformance.native(
      function(err, collection){
        if(err){
          
          console.log("Error creating native query for FundPerformance");
          return res.json(
                  "Error Fetching FundPerformance Details",
                  400
                );

        } else {
          collection.findOne(
            {
              fundId: fundId
            },
            function(err, fundPerformanceDetails){
              if(err){
              
                console.log("Error Finding FundPerformance!");
                return res.json(
                  "Error Fetching FundPerformance Details",
                  400
                );
              
              } else {

                return res.json(
                  fundPerformanceDetails,
                  200
                ); 

              }
            }
          );
        }
      }
    );
  },

  getFundNavs: function(req, res){

    var fundId = req.param('fundId');
    console.log('Fetching FundNav for FundId: '+fundId);

    var queryJson = {};

    var one_day=1000*60*60*24; // This is one day in milliseconds

    if(req.param('duration') && req.param('value')) {
      
      var duartion = req.param('duration');
      var value = req.param('value');

      var end = (new Date).getTime();

      if(duration == 'year' && value < 6) {

        var timeSpan = value * 365 * one_day; // years in milliseconds
        var start = end - timeSpan; 

        queryJson = {
          fundId: fundId,
          date: {
            $gte: start,
            $lt: end
          }
        };

      } else if (duration == 'month' && value < 12) {

        var start = end;

        if(end.getMonth() > value) {
          start.setMonth(end.getMonth() - value)
        } else {
          start.setYear(end.getYear() - 1);
          start.setMonth(12 + end.getMonth() - value);
        }

        queryJson = {
          fundId: fundId,
          date: {
            $gte: start,
            $lt: end
          }
        };

      } else {

        // Fetching default Navs
        var end = (new Date).getTime();
        var start = end - (30*one_day); // Thirty days in milliseconds

        queryJson = {
          fundId: fundId,
          date: {
            $gte: start,
            $lt: end
          }
        };
      }
    
    } else if(req.param('from') && req.param('to')) {

      var start = req.param('from');
      var end = req.param('to');

      queryJson = {
          fundId: fundId,
          date: {
            $gte: start,
            $lt: end
          }
        };

    } else if(req.param('from')) {

      // Fetch navs from start time to next 30 days
      var start = req.param('from');
      var to = start + 30*one_day;

       queryJson = {
          fundId: fundId,
          date: {
            $gte: start,
            $lt: end
          }
        };

    } else if(req.param('to')) {

      // Fetch navs till end time since 30 days before end time.
      var end = req.param('to');
      var start = to - 30*one_day;

       queryJson = {
          fundId: fundId,
          date: {
            $gte: start,
            $lt: end
          }
        };

    } else {

      // No paramter specified, fetcing the last months Navs by default
      var end = (new Date).getTime();
      var start = end - (30*one_day); // Thirty days in milliseconds

      queryJson = {
        fundId: fundId,
        date: {
          $gte: start,
          $lt: end
        }
      };
    }

    FundNav.native(
      function(err, collection){
        if(err){
          
          console.log("Error creating native query for FundNav");
          return res.json(
                  "Error Fetching FundNav Details",
                  400
                );

        } else {
          collection.findOne(
            {
              fundId: fundId
            },
            function(err, fundNavDetails){
              if(err){
              
                console.log("Error Finding FundNav!");
                return res.json(
                  "Error Fetching FundNav Details",
                  400
                );
              
              } else {

                return res.json(
                  fundNavDetails,
                  200
                ); 

              }
            }
          );
        }
      }
    );
  }
  

 /* create: function(req, res){

    var request = req.body;
    console.log('Creating Fund: '+JSON.stringify(request));

    Fund.create(request).done(
      function(err, persistedFund) {

          // Error handling
          if (err) {
            // The Fund could not be created!
            console.log("Error Persisting Fund: " + JSON.stringify(err));
            return res.json(
              {
                success: false,
                message: "Error Persisting Fund"
              }
            );
          } else {
            //Fund was created successfully!
            
            console.log("Success Persisting Fund: " + JSON.stringify(persistedFund));
            return res.json(
              persistedFund,
              201
            );
          }
      }
    );
  },

  find : function (req, res) { 
      console.log("Fetching Fund Details: " + JSON.stringify(req.body));

      var id = req.param('id');

       Fund.native(
        function(err, collection){
          collection.findOne(
            {
              _id: new ObjectId(id)
              //"isDeleted": false
            },
            {
              followers: 0,
              backers: 0,
              referrals: 0
            },
            function(err, fund){
              if(err){
                  // Fund could not be found!
                  console.log("Error finding Fund: "+ JSON.stringify(err));
                  return res.json(
                      {
                        success: false,
                        message: "Error finding Fund"
                      },
                      404
                    );
              } else {
                return res.json(
                  fund,
                  200
                );
              }
            }
          )
        }
      );
    },

    findName : function (id,callback) { 

      console.log("Fetching Fund Name : " + id);

      Fund.native(
        function(err, collection){
          collection.findOne(
            {
              _id: new ObjectId(id),
              //"isDeleted": false
            },
            {
              name : 1
            },
            function(err, fund){
              if(err){
                  // User could not be found!
                  console.log("Error finding User: " + JSON.stringify(err));
                  return res.json(
                      {
                        success: false,
                        message: "Error finding User"
                      },
                      404
                    );
              } else {
                if(fund)
                  callback(fund.name);
                else
                  callback(fund);
              }
            }
          )
        }
      );
    },
*/
    /*list: function(req, res) {
      
      console.log("Fetching Fund " + JSON.stringify(req.body));
*/
      /*
      * Paginating the funds list using page number and 
      * number of documents per page. Combined aprroach - range by "_id" + skip()
      * Using skip+limit is not a good way to do paging when performance is an issue,
      * or with large collections; it will get slower and slower as you increase the page number.
      * Using skip requires the server to walk though all the documents (or index values) from 0 
      * to the offset (skip) value. It is much better to use a range query (+ limit) where you pass
      * in the last page's range value.
      */
/*
      var pageSize = req.param("size");
      var currentPageNumber = req.param("pageFrom");
      var newPageNumber = req.param("pageTo");
      var currentId = req.param("currentId"); // id of the first record on the current page
      var pageDiff = currentPageNumber - newPageNumber;
      var skip;

      // Loading Deafults
      if(!currentId)
        currentId = 0;
      if(!pageSize)
        pageSize = 10;
      if(!pageDiff)
        skip = 0;

      // Calculating the number of documents to skip and the queryParameter
      var queryParameter = {};
      if(pageDiff < 0) {

        if(!skip)
          skip = pageSize * ( ( (-1) * pageDiff) - 1);

        queryParameter = {
                          _id : 
                            {
                              $lt: new ObjectId(currentId)
				//, "isDeleted": false
                            }
                        }
      } else {
      
        if(!skip)
          skip = pageSize * pageDiff;
        
        queryParameter = {
                          _id : 
                            {
                              $gt: new ObjectId(currentId)
				//, "isDeleted": false
                            }
                        }
      }

      // Fetching Fund List
      Fund.native(
        function(err,collection){
          if(err){
            console.log("Error making native query: " + JSON.stringify(err));
            return res.json(
              {
                success: false,
                message: "Error making native query"
              }
            );
          } else {
              collection.find(
                queryParameter,
                {
                  name: 1,
                  aum: 1,
                  email: 1,
                  logoImage: 1
                }
              )
              .skip(skip)
              .limit(parseInt(pageSize))
              .sort({_id: 1}) // Sort by Id
              .toArray(
                function(err, funds){
                  if(err){
                    // Funds could not be fetched!
                    console.log("Error Fetching Funds: "+ JSON.stringify(err));
                    return res.json(
                      {
                        success: false,
                        message: "Error Fetching Funds"
                      },
                      404
                    );
                  } else {
                    return res.json(
                      funds,
                      200
                    );
                  }
                }
              );
            }
          }
        );
    },
    update : function (req, res) { 
      console.log("Updating Fund Details: " + JSON.stringify(req));

      var id = req.params('id');
      var updateParameters = req.body;

       Fund.native(
        function(err, collection){
          collection.update(
            {
              _id: new ObjectId(id)
            },
            {
              $set: updateParameters
            },
            function(err, fund){
              if(err){
                  // Fund could not be updated!
                  console.log("Error updating Fund: "+ JSON.stringify(err));
                  return res.json(
                      {
                        success: false,
                        message: "Error updating Fund"
                      }
                    );
              } else {
                  return res.json(
                    {
                      success: true,
                      message: "Success Updating Fund",
                    }
                  );
              }
            }
          )
        }
      );
    },

    destroy : function (req, res) { 
      
      console.log("Soft Deleting Fund : " + JSON.stringify(req));

      var id = req.params('id');

      // Soft deleting Fund
      Fund.native(
        function(err, collection){
          collection.update(
            {
              _id: new ObjectId(id)
            },
            {
              $set: {"isDeleted": true}
            },
            function(err, fund){
              if(err){
                  // Fund could not be soft deleted!
                  console.log("Error soft deleting Fund: "+ JSON.stringify(err));
                  return res.json(
                      {
                        success: false,
                        message: "Error soft deleting Fund"
                      }
                    );
              } else {
                return res.json(
                  {
                    success: true,
                    message: "Success soft deleting Fund",
                  }
                );
              }
            }
          )
        }
      );
    },

	updateReferrals : function(req,res) {
		console.log('Content for updating referrals : '+ JSON.stringify(req));
		var id = req.id;

		Fund.native(
			function(err,collection){
  			collection.update(
  				{
  					_id: new ObjectId(id)
  				},
  				{
  					$addToSet: { 
  						refer: {
  							"initiator":req.initiator,
  							"initiated":req.initiated
  						} 
  					}
  				},
  				function(err,updatedFund){
  					if(err){
  						// The Fund could not be updated!
	    				console.log("Error Updating Fund: "+err);
	  					return res.json(
  							{
  								success: false,
  								message: "Error Updating Fund"
  							}
  						);
  					}
  					else {
  						// Fund was successfully updated!
	    				console.log("Success Updating Fund: " + updatedFund);
	  					return res.json(
  							{
  								success: true,
  								message: "Success Updating Fund"
  							}
  						);
  					}
				  }
  			);
	  	}
	  );
	},

  addFollower : function (req, res) {
    
    console.log("Adding follower: "+ JSON.stringify(req));

    var followerId = req.followerId;
    var id = req.id;

    // Adding followee's Id to the Follower's following.. This is soo confusing!
    Fund.native(
      function(err,collection){
        collection.update(
          {
            _id: new ObjectId(id)
          },
          {
            $addToSet: { 
              followers: followerId
            }
          },
          function(err,updatedFund){
            if(err){
              // Fund could not be updated!
              console.log("Error Adding Follower to Fund: "+ JSON.stringify(err));
              return res.json(
                {
                  success: false,
                  message: "Error Adding Follower"
                }
              );
            }
            else {
              // Fund was successfully updated!
              console.log("Success Adding Follower to Fund: " + updatedFund);
              return res.json(
                {
                  success: true,
                  message: "Success Adding Follower"
                }
              );
            }
          }
        );
      }
    );

    // Adding follower's Id to the Followee's follower.. This is soo confusing!
    User.native(
      function(err,collection){
        collection.update(
          {
            _id: new ObjectId(followerId)
          },
          {
            $addToSet: { 
              following: {
                id: id,
                collection: "Fund"
              }
            }
          },
          function(err,updatedUser){
            if(err){
              // User could not be updated!
              console.log("Error Adding Following to User: "+ JSON.stringify(err));
              return res.json(
                {
                  success: false,
                  message: "Error Adding Following"
                }
              );
            }
            else {
              // User was successfully updated!
              console.log("Success Adding Following to User: " + updatedUser);
              return res.json(
                {
                  success: true,
                  message: "Success Adding Following"
                }
              );
            }
          }
        );
      }
    );
  },
  removeFollower : function (req, res) {
    
    console.log("Removing follower: " + JSON.stringify(req));

    var followerId = req.followerId;
    var id = req.id;

    // Removing followee's Id to the Follower's following.. This is soo confusing!

    Fund.native(
      function(err,collection){
        collection.update(
          {
            _id: new ObjectId(id)
          },
          {
            $pull: { 
              followers: followerId
            }
          },
          function(err,updatedFund){
            if(err){
              // Fund could not be updated!
              console.log("Error Removing Following from Fund: "+ JSON.stringify(err));
              return res.json(
                {
                  success: false,
                  message: "Error Removing Following"
                }
              );
            }
            else {
              // Fund was successfully updated!
              console.log("Success Removing Following from Fund: " + updatedFund);
              return res.json(
                {
                  success: true,
                  message: "Success Removing Following"
                }
              );
            }
          }
        );
      }
    );

    // Removing follower's Id to the Followee's follower.. This is soo confusing!
    User.native(
      function(err,collection){
        collection.update(
          {
            _id: new ObjectId(followerId)
          },
          {
            $pull: { 
              following: {
                id: id,
                collection: "Fund"
              }
            }
          },
          function(err,updatedUser){
            if(err){
              // User could not be updated!
              console.log("Error Removing Follower from User: "+ JSON.stringify(err));
              return res.json(
                {
                  success: false,
                  message: "Error Removing Follower"
                }
              );
            }
            else {
              // User was successfully updated!
              console.log("Success Removing Follower from User: " + updatedUser);
              return res.json(
                {
                  success: true,
                  message: "Success Removing Follower"
                }
              );
            }
          }
        );
      }
    );
  },
  updateLikesCount: function(req, res) {

		var id = req.id;

    // Integer value by which numberOfLikes is to be updated
    // This could be either a positive or a negative value
    // Generally it is supposed to be 1 or -1 
		var incrementBy = req.counter;

		Fund.native(
      function(err,collection){
  			collection.update(
  				{
  					_id: new ObjectId(id)
  				},
  				{
					  $inc: { numberOfLikes: incrementBy }
				  },
  				function(err,updatedFund){
  					if(err){
  						// Fund could not be Updated!
	    				console.log("Error Updating Fund: " + JSON.stringify(err));
	  					return res.json(
                {
							    success: false,
							    message: "Error Updating Fund"
						    }
              );
  					}
  					else {
  						// Fund was successfully Updated!
	    				console.log("Success Updating Fund: " + updatedFund);
	  					return res.json(
                {
								  success: true,
								  message: "Success Updating Fund"
							 }
              );
  					}
  				}
  			);
  		}
    );
	},
	addBacker: function(req, res) {

		var id = req.id;
		var backerId = req.backerId;
		var amountInvested = req.amount;
		var fundHouseId = req.fundHouseId;


		Fund.native(
      function(err,collection){
  			collection.update(
  				{
  					_id: new ObjectId(id)
  				},
  				{
  					$push: { 
  						backers: {
  							id : backerId,
  							amount : amountInvested,
  							date : new Date()
  						} 
  					}
  				},
  				function(err,updatedFund){
  					if(err){
  						// Fund could not be Updated!
	    				console.log("Error adding backer to Fund: " + JSON.stringify(err));
	  					return res.json(
                {
							    success: false,
							    message: "Error adding backer to Fund"
						    }
              );
  					}
  					else {
  						// Fund was successfully Updated!
	    				console.log("Success adding backer to Fund: " + updatedFund);

	    				var addBackerToFundHouseParams = {};
	    				addBackerToFundHouseParams.id = fundHouseId;
	    				addBackerToFundHouseParams.backerId = req.backerId;
	    				addBackerToFundHouseParams.amount = req.amount;

              // Adding backer to the FundHouse of the Fund
	    				FundHouseController.addBacker(addBackerToFundHouseParams,res);

	  					return res.json(
                {
								  success: true,
								  message: "Success adding backer to Fund"
							  }
              );
  					}
  				}
  			);
  		}
    );
	},
  fetchFollowers : function (req, res) {

    var request = req.body;
    var id = req.param("id");

    // Paginating the followers list using page number and 
    // number of documents per page
    var pageSize = req.param("size");
    var page = pageSize;
    var pageNumber = req.param("page");

    // Calculating the number of documents to skip
    var skip = pageSize * (pageNumber - 1);

    Fund.native(
      function(err,collection){
         collection.findOne(
          {
            _id: new ObjectId(id)
          },
          {
            follower: {
              $slice:[
                skip,
                parseInt(pageSize)
              ]
            },
            _id:0,
            team:0,
            tags:0,
            qna:0,
            pitch:0,
            backers:0,
            refer:0,
            terms:0
          },
          function(err, fund){

            if(err){
              // Fund could not be found!
              console.log("Error finding Fund: "+ JSON.stringify(err));
              return res.json(
                  {
                    success: false,
                    message: "Error finding Fund"
                  }
                );
            } else{

              // Fund found!
              console.log("Success finding Fund: " + JSON.stringify(fund));
             
              var followerIds = [];
              for(var i=0; i<fund.follower.length;i++){
                followerIds.push(new ObjectId(fund.follower[i]));
              }

              User.native(
                function(err,collection){
                  
                  collection.find(
                    {
                      _id: { "$in" : followerIds }
                    },
                    {
                      name: 1,
                      imageLink:1
                    }
                  )
                  .toArray(
                    function(err, followers){
                      if(err){
                        // Followers could not be fetched
                        console.log("Error Fetching Followers: "+ JSON.stringify(err));
                        return res.json(
                          {
                            success: false,
                            message: "Error Fetching Followers"
                          }
                        );
                      } else {
                        return res.json(
                          {
                            success: true,
                            message: "Success Fetching Followers",
                            followers: followers
                          }
                        );
                      }
                    }
                  );
                }
              );
            }
          }
        )
      }
    );
  },
  fetchBackers : function (req, res) {

    var request = req.body;
    var id = req.param("id");

    // Paginating the backers list using page number and 
    // number of documents per page
    var pageSize = req.param("size");
    var page = pageSize;
    var pageNumber = req.param("page");

    // Calculating the number of documents to skip
    var skip = pageSize * (pageNumber - 1);

    Fund.native(
      function(err,collection){
        collection.findOne(
          {
            _id: new ObjectId(id)
          },
          {
            backers: {
              $slice:[
                skip,
                parseInt(pageSize)
              ]
            },
            _id:0,
            team:0,
            tags:0,
            qna:0,
            pitch:0,
            followers:0,
            refer:0,
            terms:0
          },
          function(err, fund){

            if(err){
              // Fund could not be found!
              console.log("Error finding Fund: "+ JSON.stringify(err));
              return res.json(
                {
                  success: false,
                  message: "Error finding Fund"
                }
              );
            } else{

              // Fund found!
              console.log("Success finding Fund: " + JSON.stringify(fund));
              
              // Generating ObjectIds from string ids of backers 
              // for querying database and storing them in an array
              var backerIds = [];
              for(var i=0; i < fund.follower.length; i++){
                backerIds.push(new ObjectId(fund.follower[i]));
              }

              User.native(
                function(err,collection){
                  collection.find(
                    {
                      _id: { "$in" : backerIds }
                    },
                    {
                      name: 1,
                      imageLink:1
                    }
                  )
                  .toArray(
                    function(err, backers){
                      if(err){
                        // Backers could not be fetched
                        console.log("Error Fetching Backers: "+ JSON.stringify(err));
                        return res.json(
                            {
                              success: false,
                              message: "Error Fetching Backers"
                            }
                          );
                      } else {
                        return res.json(
                          {
                            success: true,
                            message: "Success Fetching Backers",
                            backers: backers
                          }
                        );
                      }
                    }
                  );
                }
              );
            }
          }
        )
      }
    );
  }*/
};