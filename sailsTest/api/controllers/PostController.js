var NotificationController  = require('./NotificationController');
var NewsFeedController  = require('./NewsFeedController');
var ObjectId = require('mongodb').ObjectID;


module.exports = {

	create: function(req, res){

		console.log('Creating Post : '+ JSON.stringify(req.body));

		var newPost = req.body;

		// Adding post object in the database
		Post.create(newPost).done(
			function(err, persistedPost) {

				// Error handling
  				if (err) {
    				// The Post could not be created!
    				console.log("Error Persisting Post: " + JSON.stringify(err));
  					return res.json(
  						{
  							success: false,
  							message: "Error Persisting Post"
  						}
  					);
  				} else {
  					//Post was created successfully!
    				console.log("Post created:", persistedPost);

    				var newNotification;

    				if(persistedPost.postDestinationOwner) {

						newNotification = {};
						console.log("Creating Notification for User: "+ persistedPost.postDestinationOwner)
						
						newNotification.userId = persistedPost.postDestinationOwner;
						newNotification.collection = persistedPost.parentCollection;
						newNotification.collectionId = persistedPost.id;
						newNotification.postInitiator = persistedPost.initiator;

						console.log("New Notification: " + JSON.stringify(newNotification));

					}
					
					if(newNotification) {
						req.body = newNotification;
						NotificationController.create(req,res);
					} else {
						console.log("No notification created!");
					}

					var feed = persistedPost;
					feed.type = "Post";
					req.body = feed;
					NewsFeedController.create(req, res);

    				console.log("Success Persisting Post: "+persistedPost);
  					return res.json(
  						persistedPost,
  						201
  					);
	  			}
			}
		);
	},

	find : function (req, res) { 
      console.log("Fetching Post Details: " + JSON.stringify(req.body));

      var id = req.param('id');

       Post.native(
        function(err, collection){
          collection.findOne(
            {
              _id: new ObjectId(id)
            },
            function(err, post){
              if(err){
                  // Post could not be found!
                  console.log("Error finding Post: "+ JSON.stringify(err));
                  return res.json(
                      {
                        success: false,
                        message: "Error finding Post"
                      }
                    );
              } else {
                return res.json(
                  post,200
                );
              }
            }
          )
        }
      );
    },

	update: function(req, res){

		console.log('Updating Post : '+ JSON.stringify(req.body));

		var id = req.param('id');

		var content = req.body.content;

		Post.native(
            function(err,collection){
            
                collection.update(
                    {
                        _id: new ObjectId(messageId)
                    },
                    {
                    $set: { content: content }
                    },
                    function(err,updatedPost){
                        if(err){
                            // The Post could not be Updated!
                            console.log("Error Updating Post: "+err);
                            return res.json(
                                {
                                    success: false,
                                    message: "Error Updating Post"
                                }
                            );
                        }
                        else {
                            // Post was successfully Updated!
                            console.log("Success Updating Post: " + updatedPost);
                            return res.json(
                                {
                                    success: true,
                                    message: "Success Updating Post"
                                }
                            );
                        }
                    }
                );
            }
        );
	},

	destroy: function(req, res){
		
		var id = req.param('id');
		var removeNotificationQueryJson = {};

		removeNotificationQueryJson.collection = "Post";
	  	removeNotificationQueryJson.collectionId = id;

		// Removing a Post, this is not soft delete
		Post.native(
			function(err,collection){
	  			collection.remove(
	  				{
	  					_id: new ObjectId(id)
	  				},
	  				function(err,removedPost){
	  					if(err){
	  						// The Post could not be Removed!
		    				console.log("Error Removing Post: "+ JSON.stringify(err));
		  					return res.json(
		  						{
		  							success: false,
		  							message: "Error Removing Post"
		  						}
		  					);
	  					}
	  					else {
	  						
	  						if(removedPost > 0) {
	  							// Post was successfully Removed!
		  						var queryJson = {
				  					parentId: id
				  				};

								var removeEntityRequest = {};
								removeEntityRequest.queryJson = queryJson;

								// Removing activities and comments related to the post
								sails.controllers.activity.destroy(removeEntityRequest,res);
								sails.controllers.comment.destroy(removeEntityRequest,res);

								// Removing notifications related to the post
								var removeEntityRequest = {};
								removeEntityRequest.queryJson = removeNotificationQueryJson;
								sails.controllers.notification.destroy(removeEntityRequest,res);

			    				console.log("Success Removing Post: " + removedPost);
			  					return res.json(
			  						{
			  							success: true,
			  							message: "Success Removing Post"
			  						}
			  					);	
	  						} else {
	  							// Post was not Removed!
		    					console.log("No Post Removed/Found" );
		    					return res.json(
		  							{
		  								success: false,
		  								message: "No Post Removed/Found"
		  							}
		  						);
	  						}
	  						
	  					}
	  				}
	  			);
	  		}
		);
	},

	updateLikesCount: function(req, res) {
		
		var id = req.id;

		// The counter by which number of likes is to be updated,
		// This could be a positive as well as a negative value.
		// Generally it would be 1 or -1
		var incrementBy = req.counter;

		Post.native(
			function(err,collection){
	  			collection.update(
	  				{
	  					_id: new ObjectId(id)
	  				},
	  				{
					$inc: { numberOfLikes: incrementBy }
					},
	  				function(err,updatedPost){
	  					if(err){
	  						// The Post could not be Updated!
		    				console.log("Error Updating Post: "+err);
		  					return res.json(
		  						{
		  							success: false,
		  							message: "Error Updating Post"
		  						}
		  					);
	  					}
	  					else {
	  						// Post was successfully Updated!
		    				console.log("Success Updating Post: " + updatedPost);
		  					return res.json(
		  						{
		  							success: true,
		  							message: "Success Updating Post"
		  						}
		  					);
	  					}
	  				}
	  			);
	  		}
	  	);
	},

	updateSharesCount: function(req, res) {
		
		var id = req.id;

		// The counter by which number of shares is to be updated,
		// This could be a positive as well as a negative value.
		// Generally it would be 1 or -1
		var incrementBy = req.counter;

		Post.native(
			function(err,collection){
	  		
	  			collection.update(
	  				{
	  					_id: new ObjectId(id)
	  				},
	  				{
						$inc: { numberOfShares: incrementBy }
					},
	  				function(err,updatedPost){
	  					if(err){
	  						// The Post could not be Updated!
		    				console.log("Error Updating Post: "+err);
		  					return res.json(
		  						{
		  							success: false,
		  							message: "Error Updating Post"
		  						}
		  					);
	  					}
	  					else {
	  						// Post was successfully Updated!
		    				console.log("Success Updating Post: " + updatedPost);
		  					return res.json(
		  						{
		  							success: true,
		  							message: "Success Updating Post"
		  						}
		  					);
	  					}
	  				}
	  			);
	  		}
	  	);

	},

	updateCommentsCount: function(req, res) {
	
		var id = req.id;

		// The counter by which number of comments is to be updated,
		// This could be a positive as well as a negative value.
		// Generally it would be 1 or -1
		var incrementBy = req.counter;

		Post.native(
			function(err,collection){
	  		
	  			collection.update(
	  				{
	  					_id: new ObjectId(id)
	  				},
	  				{
						$inc: { numberOfComments: incrementBy }
					},
	  				function(err,updatedPost){
	  					if(err){
	  						// The Post could not be Updated!
		    				console.log("Error Updating Post: "+err);
		  					return res.json(
		  						{
		  							success: false,
		  							message: "Error Updating Post"
		  						}
		  					);
	  					}
	  					else {
	  						// Post was successfully Updated!
		    				console.log("Success Updating Post: " + updatedPost);
		  					return res.json(
		  						{
		  							success: true,
		  							message: "Success Updating Post"
		  						}
		  					);
	  					}
	  				}
	  			);
	  		}
	  	);
	},

	fetchPosts: function(req, res) {

	    var userId = req.param("userId");

	    console.log("Fetching posts for user: "+ userId);

	    /*
	    * Paginating the users list using page number and 
	    * number of documents per page. Combined aprroach - range by "_id" + skip()
	    * Using skip+limit is not a good way to do paging when performance is an issue,
	    * or with large collections; it will get slower and slower as you increase the page number.
	    * Using skip requires the server to walk though all the documents (or index values) from 0 
	    * to the offset (skip) value. It is much better to use a range query (+ limit) where you pass
	    * in the last page's range value.
	    */

    	var pageSize = req.param("size");
      	var currentPageNumber = req.param("pageFrom");
      	var newPageNumber = req.param("pageTo");
      	var currentId = req.param("currentId"); // id of the first record on the current page
      	var pageDiff = currentPageNumber - newPageNumber;
      	var skip;

      	// Loading Deafults
      	if(!currentId)
        	currentId = 0;
      	if(!pageSize)
        	pageSize = 10;
      	if(!pageDiff)
        	skip = 0;

      	// Calculating the number of documents to skip and the queryParameter
      	var queryParameter = {};
      	if(pageDiff < 0) {

        if(!skip)
          	skip = pageSize * ( ( (-1) * pageDiff) - 1);

        	queryParameter = {
                          	_id : 
                            	{
                              		$lt: new ObjectId(currentId)
                            	}
                        	}
      
      	} else {
      
        	if(!skip)
          		skip = pageSize * pageDiff;
        
        	queryParameter = {
                          	_id : 
                            	{
                            	  	$gt: new ObjectId(currentId)
                            	}
                        	}
      	}

      	console.log("Query Parameter: \n" + JSON.stringify(queryParameter));

	    // Fetching Posts
        Post.native(
          	function(err,collection){
	            if(err){
	              console.log("Error making native query: "+JSON.stringify(err));
	              return res.json(
	                {
	                  success: false,
	                  message: "Error making native query"
	                }
	              );
	            } else {
	              	collection.find(
	              		queryParameter
	              		/*{
	              			$or: [  
		              			{
		              				initiator : userId,
		              				postType : "Post"
		              			},
		              			{
		              				postDestinationOwner : userId,
		              				postType : "Post"
		              			}
		              		]
		              	}*/
	              	)
	              	.skip(skip)
	              	.limit(parseInt(pageSize))
	            	.sort({_id: -1}) // Sort by Id
		            .toArray(
		                function(err, posts){
		                  if(err){
		                    // Posts could not be fetched!
		                    console.log("Error Fetching Posts: "+ JSON.stringify(err));
		                    return res.json(
		                    	{
		                      		success: false,
		                     		 message: "Error Fetching Posts"
		                    	}
		                  	);
		                  } else {
		                  	console.log("Posts: \n" + JSON.stringify(posts));
		                  	if(posts.length > 0){

		                  		var counter = 0;

		                  		sails.controllers.user.findName(
                    				userId,
                    				function(userDetails){

                    					console.log("UserDetails: "+JSON.stringify(userDetails));

                    					for(var i=0; i < posts.length; i++){

                    						if(posts[i].postDestinationOwner){
                    							if(userDetails) {
                    								posts[i].postDestinationOwnerName = userDetails.name;
                    								posts[i].postDestinationOwnerImage = userDetails.imageLink;
                    							}

                    							var request = {};
                    							request.index = i;
                    							request.id = posts[i].initiator;

                    							sails.controllers.user.findName(
                    								request,
                    								function(initiatorDetails, index){
                    									if(initiatorDetails) {
                    										posts[index].initiatorName = initiatorDetails.name;
                    										posts[index].initiatorImage = initiatorDetails.imageLink;
                    									}
                    									
                    									counter++;

                    									if(counter == posts.length){
				                    						return res.json(
										                       posts,
														       200 
										                    );
					                    				}	
                    								}
                    							);

                    						} else {

                    							if(userDetails) {
                    								posts[i].initiatorName = userDetails.name;
                    								posts[i].initiatorImage = userDetails.imageLink;
                    							}
                    							counter++;

                    							if(counter == posts.length){
		                    						return res.json(
								                       posts,
												       200 
								                    );
			                    				}			
                    						}
                    					}

                    				}
                    			);
		                  	}
		                  	else {
		                  		return res.json(
					                posts,
									200 
					            );
		                  	}
		                  }
		                }
		            );
	            }
          	}
        );
	},

	fetchIdeas: function(req, res) {

		var request = req.body;
	    var userId = req.param("userId");

	    // Paginating the followers list using page number and 
	    // number of documents per page
	    var pageSize = req.param("size");
	    var pageNumber = req.param("page");

	    // Calculating the number of documents to skip
	    var skip = pageSize * (pageNumber - 1);

	    // Fetching Ideas
        Post.native(
          	function(err,collection){
	            if(err){
	              console.log("Error making native query: "+JSON.stringify(err));
	              return res.json(
	                {
	                  success: false,
	                  message: "Error making native query"
	                }
	              );
	            } else {
	              	collection.find(
	              		{
	              			initiator : userId,
	              			postType : "Idea"
	              		}
	              	)
	              	.skip(skip)
	              	.limit(parseInt(pageSize))
	            	.sort({_id: 1}) // Sort by Id
		            .toArray(
		                function(err, ideas){
		                  if(err){
		                    // Ideas could not be fetched!
		                    console.log("Error Fetching Ideas: "+ JSON.stringify(err));
		                    return res.json(
		                    	{
		                      		success: false,
		                     		 message: "Error Fetching Ideas"
		                    	}
		                  	);
		                  } else {
		                    
		                    return res.json(
		                        {
		                          	success: true,
		                         	message: "Success Fetching Posts",
		                          	ideas: ideas
		                        }
		                    );
		                  }
		                }
		            );
	            }
          	}
        );
	},

	getDetails : function (id,callback) {

		console.log("\n\nId: "+id);

		Post.native(
	      function(err, collection){
	        collection.findOne(
	          {
	            _id: new ObjectId(id)
	          },
	          {
	          	initiator : 1,
	          	postType : 1,
	          	sharedPostId : 1
	          },
	          function(err, post){
	            if(err){
	                // Post could not be found!
	                console.log("Error finding Post: " + JSON.stringify(err));

	            } else {

	            	sails.controllers.user.findName(
	            		post.initiator,
	            		function(initiatorName){
	            			if(initiatorName){

	            				post.initiatorName = initiatorName;

	            				callback(post);

	            			} else {
	            				console.log("Error Finding User Name");
	            				callback()
	            			}

	            		}
	            	)
	              	
	            }
	          }
	        )
	      }
	    );

	}
};