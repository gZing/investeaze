// Imports
var Pusher = require('pusher');
var ObjectId = require('mongodb').ObjectID;

// Initializing
var pusher = new Pusher({
  appId: '77682',
  key: '1d21f8b62586ce0ac86f',
  secret: '83214175fc4c5379c17f'
});


module.exports = {
	/*
	* This method allows a user to connect to a message socket.
	* If the method call is successful, the user is added onlineUsers gloabal list.
	*/
	connect: function(req, res){

		// Fetching socket from the request
		var socket = req.socket;

		// UserId which is sent from the client side
		var userId = req.param('userId');

    	console.log('Message Socket Connected for user: ' + userId);

    	// setting username for the socket
    	socket.username = userId;

    	/*
    	* Check if the user is already active, i.e if the user already exists in the 
    	* onlineUsers list. onlineUsers list keeps the active userId as the key and 
    	* socketId as the value corresponding to it. This helps us to find out if the 
    	* recepient of a message in currently online or not, so that we can emit message to
    	* corresponding socket for the recepient.
    	*/
    	if(sails.config.globals.onlineUsers[userId])
		    	console.log("User already active");
		else {
			sails.config.globals.onlineUsers[userId] = socket.id;
		    console.log("User now active: " + userId);
		}

	    socket.on('disconnect',
	    	function(){
	    		// Socket is disconnected, we need to delete the userId from global onlineUsers list.
	    		delete sails.config.globals.onlineUsers[socket.username];
	    		console.log("User disconnected: " + socket.username);
	    	}
	    );

	    return res.json(
	    	{
	    		success : true
	    	}
	    );
	},

	/*
	* This method handles the create message request. It first inserts the message object in the database,
	* next it iterates over the recipients list, and checks for online users. If the user is online,
	* the message is emitted on its corresponding socket. 
	*/
	create: function(req, res){

		var request = req.body;
		console.log('Creating Message: '+JSON.stringify(request));

		
		if(request.to == request.from){
			// Users can't send messages to themselves

			// The Message could not be created!
			console.log("matching sender and receiver id");
			return res.json(
				{
					success: false,
					message: "matching sender and receiver id"
				}
			);

		} else {
			// Adding message object in the database
			Message.create(request).done(
				function(err, persistedMessage) {

				    // Error handling
	  				if (err) {
	    				// The Message could not be created!
	    				console.log("Error Persisting Message: " + JSON.stringify(err));
	  					return res.json(
	  						{
	  							success: false,
	  							message: "Error Persisting Message"
	  						}
	  					);
	  				} else {
	  					//Message was created successfully!
						
						//=== Sockets code
						/*if(sails.config.globals.onlineUsers[req.body.to]) {
				 		    	console.log("User " + req.body.to + " is currently logged in. Sending chat message..");
				 		    	sails.io.sockets.socket(sails.config.globals.onlineUsers[req.body.to]).emit('messageReceived', req.body);
				 		}*/

				 		//=== Pusher code

				 		/*
				 		* Sending message to the user if the user is online, i.e.
				 		* its message channel is active. We assume that the message 
				 		* channel for each user is of the form:
				 		* presence-messageChannel-userId
				 		*/

				 		var userId =  persistedMessage.to;

				 		var channelName = 'presence-messageChannel-'+userId;

				 		/*
				 		* Check for the member count of the user's message channel to 
				 		* check whether the user is online or not.
				 		*/

				 		pusher.get( { path: '/channels/'+channelName+'/users', params: {} },
						  	function( error, request, response ) {
						    	if( response && response.statusCode === 200 ) {
						      		
						      		if(JSON.parse(response.body).users.length > 0){

						      			/*
						      			* User is online, sending message to user.
						      			* We send the message at the event called ,
						      			* newMessage, and we assume that the user's
						      			* messageChannel will be binded to this event
										*/
						      			console.log('sending message to user: ' + req.body.to);

				 						pusher.trigger(
				 							channelName,
				 							'newMessage', 
				 							persistedMessage
				 						);			
				 					}
						    	}
						  	}
						);

	    				console.log("Success Persisting Message: " + JSON.stringify(persistedMessage));
	  					return res.json(
	  						persistedMessage,
	  						201
	  					);
	  				}
				}
			);

			/*
			* The following commented code if for group chat/messages
			* Will be used at a later stage
			*/

			// Iterating over the recipient's list and emitting message on online recipient's socket.
			/*for (var i=0; i < messageDetails.to.length; i++) {

				var receiverId = messageDetails.to[i];

	 		    if(sails.config.globals.onlineUsers[receiverId]) {
	 		    	
	 		    	console.log("User " + receiverId + " is currently logged in. Sending chat message..");

	 		    	sails.io.sockets.socket(sails.config.globals.onlineUsers[receiverId]).emit('messageReceived', messageDetails);
	 		    }

			}*/
		}
	},

	find : function (req, res) { 
      console.log("Fetching Message Details: " + JSON.stringify(req.body));

      var id = req.param('id');

       Message.native(
        function(err, collection){
          collection.findOne(
            {
              _id: new ObjectId(id)
            },
            {
            },
            function(err, msg){
              if(err){
                  // Message could not be found!
                  console.log("Error finding Message: "+ JSON.stringify(err));
                  return res.json(
                      {
                        success: false,
                        message: "Error finding Message"
                      }
                    );
              } else {
                return res.json(
                  {
                    success: true,
                    message: "Success finding Message",
                    msg: msg
                  }
                );
              }
            }
          )
        }
      );
    },


	// Archive a message (the messages are soft deleted)
	destroy: function(req, res){

		var request = req.body;

		var id = request.id;
		var userId = request.user;
		// If the receiver wants to archive the message or the sender
		// We expect this attribute to come from the frontend
		var isReceiver = request.isReceiver;

		if(isReceiver){
			Message.native(
				function(err,collection){
		  		
		  			collection.update(
		  				{
		  					_id: new ObjectId(id)
		  				},
		  				{
							$set: { 
								receiverArchived: true
							}
						},
		  				function(err,updatedMessage){
		  					if(err){
		  						// The Message could not be Archived!
			    				console.log("Error Archiving Message: " + err);
			  					return res.json(
		  							{
		  								success: false,
		  								message: "Error Archiving Message"
		  							}
		  						);
		  					}
		  					else {
		  						// Message was successfully archived!
			    				console.log("Success Archiving Message: " + updatedMessage);
			  					return res.json(
		  							{
		  								success: true,
		  								message: "Success Archiving Message"
		  							}
		  						);
		  					}
		  				}
		  			);
		  		}
		  	);

		} else{

			Message.native(
				function(err,collection){
		  			collection.update(
		  				{
		  					_id: new ObjectId(id)
		  				},
		  				{
							$set: { 
								archived: true
							}
						},
		  				function(err,updatedMessage){
		  					if(err){
		  						// The Message could not be Archived!
			    				console.log("Error Archiving Message: " + err);
			  					return res.json(
		  							{
		  								success: false,
		  								message: "Error Archiving Message"
		  							}
		  						);
		  					}
		  					else {
		  						// Message was successfully archived!
			    				console.log("Success Archiving Message: " + updatedMessage);
			  					return res.json(
		  							{
		  								success: true,
		  								message: "Success Archiving Message"
		  							}
		  						);
		  					}
		  				}
		  			);
		  		}
		  	);
		}
	},

	updateRead: function(req, res){

		console.log("Updating Message: "+req.param("id"));

        // messageId which is sent from the client side
        var id = req.param('id');

        Message.native(
            function(err,collection){
            
                collection.update(
                    {
                        _id: new ObjectId(id)
                    },
                    {
                    $set: { receiverRead: true }
                    },
                    function(err,updatedMessage){
                        if(err){
                            // The Message could not be Updated!
                            console.log("Error Updating Message: "+err);
                            return res.json(
                                {
                                    success: false,
                                    message: "Error Updating Message"
                                }
                            );
                        }
                        else {
                            // Message was successfully Updated!
                            console.log("Success Updating Message: " + updatedMessage);
                            return res.json(
                                {
                                    success: true,
                                    message: "Success Updating Message"
                                }
                            );
                        }
                    }
                );
            }
        );
    },

    // Fetch user mesages
    getMessages : function(req, res){

        // userId which is sent from the client side
        var userId = req.param('userId');

        // authUserId which is present in the session
        var authUserId = req.session.passport.user;

        Message.native(
        	function(err, collection){
        	collection.find(
        		{ 
        			$and : [
        				{ from : 
        					{ 
        						$in : [ authUserId, userId] 
        					}
        				}, 
	                 	{ to : 
	                 		{ 
	                 			$in : [ authUserId, userId] 
	                 		}
	                 	} 
                 	] 
                 } 
            )
            .sort({
            		createdAt : 1
            	}
            )
            .limit(5)
            .toArray(
                function(err, messages){

            		if(err){

            				return res.json(
			                  	JSON.stringify(err),
			                    400
		                 	);

            		} else {
            			if(messages.length > 0) {

            				var counter = 0;
            				var messageDetails = [];

		                	for (var i=0; i < messages.length; i++){

		                		var getFromNameRequest = {};
		                		getFromNameRequest.index = i;
		                		getFromNameRequest.id = messages[i].from;

		                		sails.controllers.user.findName(
		                    		getFromNameRequest,
		                    		function(fromName, index){

		                    			var getToNameRequest = {};
				                		getToNameRequest.index = index;
				                		getToNameRequest.id = messages[index].to;

		                    			messages[index].fromName = fromName;

		                    			sails.controllers.user.findName(

		                    				getToNameRequest,
		                    				function(toName, index){

		                    					messages[index].toName = toName;
		                    					messageDetails.push(messages[index]);

		                    					counter++;

				                    			if(counter == messages.length){
				                    				return res.json( 
								                    	messageDetails,
								                        200
								                    );
				                    			}
		                    				}
		                    			);
		                    		}
		                    	)
		                	}

            			} else {
            				return res.json(
				                  	messages,
				                    200
			                 	);
            			}

            		}
            	}
            )
        })
    },


    getInteractions : function(req, res){

        // authUserId which is present in the session
        //var authUserId = req.session.passport.user;
        var authUserId = req.param('id');

        //var authUserObjectId = new ObjectId(authUserId); 

        var numOfInteractions = 20;
        var pageno = req.param('page');

        if(req.param('page'))
        	pageno = req.param('page');
        else
        	pageno = 1; 

     
        Message.native(
        	function(err, collection){
            	collection.aggregate(
            		[
            			{ "$match" : {
            					"$or": [
            						{
	            						"from": authUserId
	            					},
	            					{
	            						"to": authUserId
	            					}
	            				]
            				} 
            			},
            	 		{ "$group" : { 
	            	 			_id : { 
	            	 				from : "$from",
	            	 				to : "$to"
	            	 			}, 
	            	 			timeStamp : {
	            	 				"$max" : "$createdAt"
	            	 			},
	            	 			content : {
	            	 				"$addToSet" : "$content"
	            	 			}
            	 			}
            	 		},
            	 		{ "$sort" : {
            	 				timeStamp : -1 
            	 			} 
            	 		},
            	 		{ 
            	 			"$skip" : numOfInteractions * (pageno-1)
            	 		},
            	 		{ 
            	 			"$limit" : numOfInteractions 
            	 		}
            	 	],
	            	function(err, list){

	            		if(err){

	            				return res.json(
				                  	JSON.stringify(err),
				                    400
			                 	);

	            		} else {
	            			if(list.length > 0){
	            				console.log("List\n: "+JSON.stringify(list));
			                	var flist = [];

			                	var counter = 0;

			                	for (var i=0; i < list.length; i++){

			                		var entry = {};
			                		if (authUserId == list[i]._id.from)
			                			entry.with = list[i]._id.to;
			                		else
			                			entry.with = list[i]._id.from;
			                		entry.timeStamp = list[i].timeStamp;
			                		entry.message = list[i].content[0];

			                		flist.push(entry);

			                		var getFromNameRequest = {};
			                		getFromNameRequest.id = entry.with;
			                		getFromNameRequest.index = i;

			                		sails.controllers.user.findName(
			                    		getFromNameRequest,
			                    		function(name, index){
			                    			flist[index].withName = name;
			                    			
			                    			counter++;

			                    			if(counter == list.length){

			                    				var uniqueNames = [];
			                    				var unqiueInteractions = [];

			                    				for(var j=0; j<flist.length; j++){
			                    					if(uniqueNames.indexOf(flist[j].withName) == -1){
			                    						unqiueInteractions.push(flist[j]);
			                    						uniqueNames.push(flist[j].withName);
			                    					}
			                    				}

			                    				return res.json( 
							                    	unqiueInteractions,
							                        200
							                    );
			                    			}
			                    		}
			                    	)
			                	}
			                } else {
			                  	return res.json(
				                  	list,
				                    200
			                 	);
			                }
	            		}
            		}
           		)
        	}
        );
    },

    
  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to UserController)
   */
  _config: {}


}