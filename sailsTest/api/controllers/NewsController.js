//Imports
var ObjectId = require('mongodb').ObjectID;

module.exports = {

  create: function(req, res){

    console.log('Creating News : '+ JSON.stringify(req.body));

    var newNews = req.body;

    // Adding news object in the database
    News.create(newNews).done(
      function(err, persistedNews) {

        // Error handling
          if (err) {
            // The News could not be created!
            console.log("Error Persisting News: " + JSON.stringify(err));
            return res.json(
              {
                success: false,
                message: "Error Persisting News"
              }
            );
          } else {

            //News was created successfully!
            console.log("Success Persisting News: " + JSON.stringify(persistedNews));

            return res.json(
              persistedNews,
              201
            );
          }
      }
    );
  },

  find : function (req, res) { 

    var id;

    // Check if req is a HTTP request or a JSON request
    if(req.id)
      id = req.id;
    else
      id = req.param('id');

    console.log("Fetching News Details: " + id);

    News.native(
      function(err, collection){
        collection.findOne(
          {
            _id: new ObjectId(id)
          },
          function(err, news){
            if(err){
                // News could not be found!
                console.log("Error finding News: " + JSON.stringify(err));
                return res.json(
                    {
                      success: false,
                      message: "Error finding News"
                    },
                    404
                  );
            } else {
              return res.json(
                news,
                200
              );
            }
          }
        )
      }
    );
  },

    list: function(req, res) {

      console.log("Fetching News " + JSON.stringify(req.body));

      var request = req.body;

      /*
      * Paginating the news list using page number and 
      * number of documents per page. Combined aprroach - range by "_id" + skip()
      * Using skip+limit is not a good way to do paging when performance is an issue,
      * or with large collections; it will get slower and slower as you increase the page number.
      * Using skip requires the server to walk though all the documents (or index values) from 0 
      * to the offset (skip) value. It is much better to use a range query (+ limit) where you pass
      * in the last page's range value.
      */

      var pageSize = req.param("size");
      var currentPageNumber = req.param("pageFrom");
      var newPageNumber = req.param("pageTo");
      var currentId = req.param("currentId"); // id of the first record on the current page
      var pageDiff = currentPageNumber - newPageNumber;
      var skip;

      // Loading Deafults
      if(!currentId)
        currentId = 0;
      if(!pageSize)
        pageSize = 10;
      if(!pageDiff)
        skip = 0;

      // Calculating the number of documents to skip and the queryParameter
      var queryParameter = {};
      if(pageDiff < 0) {

        if(!skip)
          skip = pageSize * ( ( (-1) * pageDiff) - 1);

        queryParameter = {
                          _id : 
                            {
                              $lt: new ObjectId(currentId)/*,
                              "isDeleted": false*/
                            }
                        }
      
      } else {
      
        if(!skip)
          skip = pageSize * pageDiff;
        
        queryParameter = {
                          _id : 
                            {
                              $gt: new ObjectId(currentId)/*,
                              "isDeleted": false*/
                            }
                        }
      }

      // Fetching News List
      News.native(
        function(err,collection){
          if(err){
            console.log("Error making native query: " + JSON.stringify(err));
            return res.json(
              {
                success: false,
                message: "Error making native query"
              }
            );
          } else {
              collection.find(
                queryParameter
              )
              .skip(skip)
              .limit(parseInt(pageSize))
              .sort({_id: 1}) // Sort by Id
              .toArray(
                function(err, news){
                  if(err){
                    // News could not be fetched!
                    console.log("Error Fetching News: "+ JSON.stringify(err));
                    return res.json(
                      {
                        success: false,
                        message: "Error Fetching News"
                      },
                      404
                    );
                  } else {
                    return res.json(
                        news,
                        200
                    );
                  }
                }
              );
            }
          }
        );
    },

    update : function (req, res) { 
      console.log("Updating News Details: " + JSON.stringify(req.body));

      var id = req.param('id');
      var updateParameters = req.body;

       News.native(
        function(err, collection){
          collection.update(
            {
              _id: new ObjectId(id)
            },
            {
              $set: updateParameters
            },
            function(err, news){
              if(err){
                  // News could not be updated!
                  console.log("Error updating News: "+ JSON.stringify(err));
                  return res.json(
                      {
                        success: false,
                        message: "Error updating News"
                      }
                    );
              } else {
                return res.json(
                  {
                    success: true,
                    message: "Success Updating News",
                  }
                );
              }
            }
          )
        }
      );
    },

    destroy : function (req, res) { 
      
      var id = req.param('id');

      console.log("Deleting News: " + id);

      News.native(
        function(err,collection){
          collection.remove(
            {
              _id: new ObjectId(id)
            },
            function(err,removedNews){
              if(err){
                // The News could not be Removed!
                console.log("Error Removing News: "+ JSON.stringify(err));
                return res.json(
                  {
                    success: false,
                    message: "Error Removing News"
                  }
                );
              }
              else {
                
                if(removedNews > 0) {
                  // News was successfully Removed!
                  console.log("Success Removing News: " + removedNews);

                  return res.json(
                    {
                      success: true,
                      message: "Success Removing News"
                    }
                  );  
                } else {
                  // News was not Removed!
                  console.log("No News Removed/Found" );
                  return res.json(
                    {
                      success: false,
                      message: "No News Removed/Found"
                    }
                  );
                }
                
              }
            }
          );
        }
      );
    }
  };