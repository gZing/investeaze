// Imports
var NotificationController  = require('./NotificationController');
var NewsFeedController  = require('./NewsFeedController');
var PostController  = require('./PostController');
var ObjectId = require('mongodb').ObjectID;


module.exports = {

	create: function(req, res){

		console.log('Content for activity : '+ JSON.stringify(req.body));

		var newComment = req.body;

		// Adding comment object in the database
		Comment.create(newComment).done(
			function(err, persistedComment) {

			    // Error handling
  				if (err) {
    				// The Comment could not be created!
    				console.log("Error Persisting Comment: " + JSON.stringify(err));
  					return res.json(
  							{
  								success: false,
  								message: "Error Persisting Comment"
  							}
  						);

  				} else {
    				//Comment was created successfully!
    				console.log("Comment created:", persistedComment);

    				var updateCommentsCountRequest = {};
					updateCommentsCountRequest.id = new ObjectId(persistedComment.parentId);
					updateCommentsCountRequest.counter = 1;

					PostController.updateCommentsCount(updateCommentsCountRequest,res);

					// For Notificaions
    				var i=0;
    				Comment.native(function(err,collection){
				       collection.aggregate([
				       		{ "$match": { "parentId": persistedComment.parentId } },
				           { "$group": { "_id": '$initiator' } }
				        ],function(err,docs) {
				          
				          	if(err){
			   						console.log("Error Finding comments: " + err);
			   				}
			   				
			   				else {
						        for (var i=0; i < docs.length; i++) {

						        	var initiator = docs[i]._id;
					        		
					       		    if(initiator != persistedComment.initiator ){
						 		    	console.log("Creating Notification for: " + initiator);
						 		    	var newNotification = {};
					    				newNotification.userId = initiator;
										newNotification.collection = "Comment";
										newNotification.collectionId = persistedComment.id;

										req.body = newNotification;
										NotificationController.create(req, res);
						 		    }
						        }
			   				}
				        });
				    });

    				// For News Feed
					var feed = persistedComment;
					feed.type = "Comment";
					req.body = feed;
					NewsFeedController.create(req, res);
					
  					return res.json(
  						persistedComment,
  						201
  					);
  				}
			}
		);
	},

	find : function (req, res) { 
      console.log("Fetching Comment Details: " + JSON.stringify(req.body));

      var id = req.param('id');

       Comment.native(
        function(err, collection){
          collection.findOne(
            {
              _id: new ObjectId(id)
            },
            {
            },
            function(err, comment){
              if(err){
                  // Comment could not be found!
                  console.log("Error finding Comment: "+ JSON.stringify(err));
                  return res.json(
                      {
                        success: false,
                        message: "Error finding Comment"
                      }
                    );
              } else {
                return res.json(                  
                   comment,
		   200      
                );
              }
            }
          )
        }
      );
    },

	update: function(req, res){

		console.log('Updating Comment : '+ JSON.stringify(req.body));

		var id = req.param('id');

		var content = req.body.content;

		Comment.native(
            function(err,collection){
            
                collection.update(
                    {
                        _id: new ObjectId(id)
                    },
                    {
                    $set: { content: content }
                    },
                    function(err,updatedComment){
                        if(err){
                            // The Comment could not be Updated!
                            console.log("Error Updating Comment: " + JSON.stringify(err));
                            return res.json(
                                {
                                    success: false,
                                    message: "Error Updating Comment"
                                }
                            );
                        }
                        else {
                            // Comment was successfully Updated!
                            console.log("Success Updating Comment: " + updatedComment);
                            return res.json(
                                {
                                    success: true,
                                    message: "Success Updating Comment"
                                }
                            );
                        }
                    }
                );
            }
        );
	},

	destroy: function(req, res){
		
		var postId;
		var queryJson = {};
		var removeNotificationQueryJson = {};
		
		if(req.param){
			var id = req.param('id');
			postId = req.param('postId');

			queryJson = {
	  					_id: new ObjectID(id)
	  				};

	  		removeNotificationQueryJson.collection = "Comment";
	  		removeNotificationQueryJson.collectionId = id;

		} else {
			queryJson = req.queryJson;
		}
		
		Comment.native(
			function(err,collection){
	  		
	  			collection.remove(
	  				queryJson,
	  				1,
	  				function(err,removedComment){
	  					if(err){
	  						// The Comment could not be Removed!
		    				console.log("Error Removing Comment: "+err);
		  					return res.json(
		  							{
		  								success: false,
		  								message: "Error Removing Comment"
		  							}
		  						);
	  					}
	  					else {
		    				if(removedComment > 0) {
		    			
		    					// Comment was successfully Removed!
		    					console.log("Success Removing Comment: " + removedComment);

		    					// Updating number of comments on the post
		    					var updateParameters = {};
								updateParameters.id = postId;
								updateParameters.counter = -1;
								req.body = updateParameters;
								console.log("request body: "+JSON.stringify(req.body));

								sails.controllers.post.updateCommentsCount(updateParameters,res);

								// Removing activity related to the comment
								var removeEntityRequest = {};
								removeEntityRequest.queryJson = queryJson;

								sails.controllers.activity.destroy(removeEntityRequest,res);

								// Removing Notifications related to the Comment
								if(removeNotificationQueryJson.collectionId) {
	  								var removeEntityRequest = {};
									removeEntityRequest.queryJson = removeNotificationQueryJson;
								
									sails.controllers.notification.destroy(removeEntityRequest,res);
	  							}

		  						return res.json(
		  							{
		  								success: true,
		  								message: "Success Removing Comment"
		  							}
		  						);	
		    				} else {
		    					// Comment was not Removed!
		    					console.log("No Comment Removed/Found" );
		    					return res.json(
		  							{
		  								success: false,
		  								message: "No Comment Removed/Found"
		  							}
		  						);	
		    				}
	  					}
	  				}
	  			);
	  		}
	  	);
	},

	updateLikesCount: function(req, res) {
		var postId = req.id;
		var incrementBy = req.counter;

		Comment.native(function(err,collection){
	  		
  			collection.update(
  				{
  					_id: new ObjectId(postId)
  				},
  				{
					$inc: { numberOfLikes: incrementBy }
				},
  				function(err,updatedComment){
  					if(err){
  						// The Comment could not be Updated!
	    				console.log("Error Updating Comment: "+err);
	  					return res.json({
							success: false,
							message: "Error Updating Comment"
						});
  					}
  					else {
  						// Comment was successfully Updated!
	    				console.log("Success Updating Comment: " + updatedComment);
	  					return res.json({
								success: true,
								message: "Success Updating Comment"
							});
  					}
  				}
  			);
  		});
	},

	fetchComments: function(req, res) {

		var request = req.body;
	    var postId = req.param("postId");

	    // Paginating the followers list using page number and 
	    // number of documents per page
	    var pageSize = req.param("size");
	    var pageNumber = req.param("page");

	    // Calculating the number of documents to skip
	    var skip = pageSize * (pageNumber - 1);

	    // Fetching Comments
        Comment.native(
          	function(err,collection){
	            if(err){
	              console.log("Error making native query: "+JSON.stringify(err));
	              return res.json(
	                {
	                  success: false,
	                  message: "Error making native query"
	                }
	              );
	            } else {
	              
	              	collection.find(
	              		{
	              			parentId : postId
	              		}
	              	)
	              	.skip(skip)
	              	.limit(parseInt(pageSize))
	            	.sort({_id: 1}) // Sort by Id
		            .toArray(
		                function(err, comments){
		                  if(err){
		                    // Comments could not be fetched!
		                    console.log("Error Fetching Comments: "+ JSON.stringify(err));
		                    return res.json(
		                    	{
		                      		success: false,
		                     		 message: "Error Fetching Comments"
		                    	}
		                  	);

		                  } else{

		                    if(comments.length == 0){
                                console.log("No comments: "+ JSON.stringify(comments));
                                return res.json(
                                    [],
                                    200
                                );
                            }
		                    var counter = 0;

			                for(var i=0; i < comments.length; i++){
			                  sails.controllers.user.findName(
			                    comments[i].initiator,
			                    function(name){
			                      
			                      comments[counter].initiatorName = name;
			                      counter++;

			                      if(counter == comments.length){
			                        return res.json( 
			                          comments,
			                            200
			                        );
			                      }
			                    }
			                  )
			                }
		                  }
		                }
		            );
	            }
          	}
        );
	},

	getDetails : function (id,callback) {

		Comment.native(
	      function(err, collection){
	        collection.findOne(
	          {
	            _id: new ObjectId(id)
	          },
	          {
	          	initiator : 1,
	          	postType : 1
	          },
	          function(err, post){
	            if(err){
	                // Post could not be found!
	                console.log("Error finding Post: " + JSON.stringify(err));

	            } else {

	            	sails.controllers.user.findName(
	            		post.initiator,
	            		function(initiatorName){
	            			if(initiatorName){

	            				post.initiatorName = initiatorName;

	            				callback(post);

	            			} else {
	            				console.log("Error Finding User Name")
	            				callback()
	            			}

	            		}
	            	)
	              	
	            }
	          }
	        )
	      }
	    );
	}
};