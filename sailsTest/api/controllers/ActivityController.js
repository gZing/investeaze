// Imports
var NotificationController  = require('./NotificationController');
var PostController  = require('./PostController');
var UserController  = require('./UserController');
var CommentController  = require('./CommentController');
var FundHouseController  = require('./FundHouseController');
var FundController  = require('./FundController');
var ObjectId = require('mongodb').ObjectID;

module.exports = {

	create: function(req, res){

		console.log('Content for activity : '+ JSON.stringify(req.body));
		var request = req.body;

		if(request.activityType == 'Unlike'){

			// Removing Like Activity
			Activity.native(
				function(err,collection){
		  			collection.remove(
		  				{
		  					parentId: request.parentId,
		  					initiator: request.initiator,
		  					activityType: "Like"
		  				},
		  				1,
		  				function(err,removedActivity){
		  					if(err){
		  						// The Activity could not be Removed!
			    				console.log("Error Removing Activity: "+err);
			  					return res.json(
		  							{
		  								success: false,
		  								message: "Error Removing Activity"
		  							}
		  						);
		  					}
		  					else {
		  						// Activity was successfully Removed!
			    				console.log("Success Removing Activity: " + removedActivity);

			    				// Updating numberOfLikes for parent entity, 
			    				// Decrementing the count by 1 (-1)
			    				var updateLikesParams = {};
							  	updateLikesParams.id = new ObjectId(request.parentId);
							  	updateLikesParams.counter = -1;
							  	req.body = updateLikesParams;
							  	
							  	if(request.parentCollection == "Post"){
							  	
							  		console.log("Decrementing Likes Counter for Post: ");
							  		PostController.updateLikesCount(req,res);
							  	
							  	} else if(request.parentCollection == "Comment"){
							  	
							  		console.log("Decrementing Likes Counter for Comment: ");
							  		CommentController.updateLikesCount(req,res);
							  	
							  	} else if(request.parentCollection == "Fund"){
							  		
							  		console.log("Decrementing Likes Counter for Fund: ");
							  		FundController.updateLikesCount(req,res);

							  	} else if(request.parentCollection == "FundHouse"){
							  	
							  		console.log("Decrementing Likes Counter for FundHouse: ");
							  		FundHouseController.updateLikesCount(req,res);
							  	
							  	}
			  					return res.json(
		  							{
		  								success: true,
		  								message: "Success Removing Activity"
		  							}
		  						);
		  					}
		  				}
		  			);
		  		}
		  	);

		} else if(request.activityType == 'Unfollow'){

			var updateFollowerRequest = {};
			updateFollowerRequest.followerId = request.parentId;
			updateFollowerRequest.id = request.initiator;

			// Removing Follow Activity
			Activity.native(
				function(err,collection){
		  		
		  			collection.remove(
		  				{
		  					parentId: updateFollowerRequest.followerId,
		  					initiator: updateFollowerRequest.id,
		  					activityType: "Follow"
		  				},
		  				1,
		  				function(err,removedActivity){
		  					if(err){
		  						// The Activity could not be Removed!
			    				console.log("Error Removing Activity: "+err);
			  					return res.json(
			  							{
			  								success: false,
			  								message: "Error Removing Activity"
			  							}
			  						);
		  					}
		  					else {
		  						// Activity was successfully Removed!
			    				console.log("Success Removing Activity: " + removedActivity);
			    				
			    				// Remvoing followers from entity, and following from the user
			    				if(request.parentCollection == 'User'){
				
									UserController.removeFollower(updateFollowerRequest,res);

								} else if(request.parentCollection == 'Fund'){
									
									FundController.removeFollower(updateFollowerRequest,res);

								} else if(request.parentCollection == 'FundHouse'){
									
									FundHouseController.removeFollower(updateFollowerRequest,res);
								
								}
			  					return res.json(
		  							{
		  								success: true,
		  								message: "Success Removing Activity"
		  							}
		  						);
		  					}
		  				}
		  			);
		  		}
		  	);

		} else {
			// Adding Activity object in the database
			Activity.create(request).done(
				function(err, persistedActivity) {

				    // Error handling
	  				if (err) {
	  					// The Activity could not be created!
	    				console.log("Error Persisting Activity: "+JSON.stringify(err));
	  					return res.json(
  							{
  								success: false,
  								message: "Error Persisting Activity"
  							}
  						);
	  				} else {
	  					// The Activity was created successfully!
	    				console.log("Activity created:", persistedActivity);

	    				var newNotification = {};
						if(persistedActivity.activityType == 'Like'){

							// We will  create a notification for the user who initiated 
							// the post/comment.
							newNotification.userId = persistedActivity.parentIdInitiator;

							var updateParametersRequest = {};
							updateParametersRequest.id = new ObjectId(persistedActivity.parentId);
							updateParametersRequest.counter = 1;

							// Updating likes count for parent entity
							if(persistedActivity.parentCollection == "Post"){
								
								console.log('Updating Likes Count')
								PostController.updateLikesCount(updateParametersRequest,res);

							} else if(persistedActivity.parentCollection == "Comment"){
								
								CommentController.updateLikesCount(updateParametersRequest,res);
							
							} else if(persistedActivity.parentCollection == "Fund"){
								
								FundController.updateLikesCount(updateParametersRequest,res);
							
							} else if(persistedActivity.parentCollection == "FundHouse"){
								
								FundHouseController.updateLikesCount(updateParametersRequest,res);
							}

						} else if(persistedActivity.activityType == 'Share'){

							// We will  create a notification for the user who initiated 
							// the post/comment.
							newNotification.userId = persistedActivity.parentIdInitiator;
							newNotification.parentId = persistedActivity.parentId;
							newNotification.sharedPostId = persistedActivity.sharedPostId;
							newNotification.initiator = persistedActivity.initiator;


							var updateParametersRequest = {};
							updateParametersRequest.id = new ObjectId(persistedActivity.parentId);
							updateParametersRequest.counter = 1;

							// Updating shares  count for Post/Idea
							PostController.updateSharesCount(updateParametersRequest,res);
						
						} else if(persistedActivity.activityType == 'Follow'){
						
							console.log("Sending Requst for follow: " + JSON.stringify(request));

							// We will  create a notification for the user who is being 
							// followed
							newNotification.userId = persistedActivity.parentId;

							var updateFollowerRequest = {};
							updateFollowerRequest.id= request.parentId;
							updateFollowerRequest.followerId = request.initiator;

							// Assuming that only a user can follow another user/fund/fundHouse
							// Adding follower to parent entity and following to the user/initiator
							if(request.parentCollection == 'User'){
								
								UserController.addFollower(updateFollowerRequest,res);

							} else if(request.parentCollection == 'Fund'){
								
								FundController.addFollower(updateFollowerRequest,res);

							} else if(request.parentCollection == 'FundHouse'){
								
								FundHouseController.addFollower(updateFollowerRequest,res);
							
							}
						
						} else if(persistedActivity.activityType == 'Refer'){

							// We will  create a notification for the user who was 
							// referred
							newNotification.userId = request.initiated;

							var updateReferralsRequest = {};
							updateReferralsRequest.initiator = request.initiator;
							updateReferralsRequest.initiated = request.initiated;
							updateReferralsRequest.id = request.parentId;

							// Add referrals to parent entity
							if(request.parentCollection == 'Fund'){
								
								FundController.updateReferrals(updateReferralsRequest,res);

							} else if(request.parentCollection == 'FundHouse'){
								
								FundHouseController.updateReferrals(updateReferralsRequest,res);
							
							}

						} else if(persistedActivity.activityType == 'Invest') {
							
							console.log("Investement Request: " + request);

							var investmentParameters = {};
							investmentParameters.id = request.initiator;
							investmentParameters.fundId = request.parentId;
							investmentParameters.amount = request.amount;

							// Routing add investment request to User Contorller
							UserController.addInvestment(investmentParameters, res);

							Fund.findOne(
								{ 
									_id : new ObjectId(request.parentId) 
								}
							).done(
								function(err, fund) {
									if (err) {
					    				console.log("Error finding fund: " + JSON.stringify(err));
					  					return res.json(
				  							{
				  								success: false,
				  								message: "Error finding fund"
				  							}
				  						);
					  				} else {
					  					
					  					var addBackerParams = {};
					  					addBackerParams.id = fund.id;
					  					addBackerParams.amount = request.amount;
					  					addBackerParams.fundHouseId = fund.fundHouseId;
					  					addBackerParams.backerId = request.initiator;

					  					FundController.addBacker(addBackerParams,res);

					  					// Creating notifications of Invest activity for 
					  					// all followers of the fund 
					  					for (var i=0; i < fund.followers.length; i++) {
								        	var follower = fund.followers[i];
							 		    	
							 		    	var newNotification = {};
						    				newNotification.userId = follower;
											newNotification.collection = "Activity";
											newNotification.collectionId = persistedActivity.id;
											
											var notificationRequest = {};
											notificationRequest.body = newNotification;
											NotificationController.create(notificationRequest, res);
							 		    
								        }
					  				}
								}
							);
							// Sending Notification to followers of initiator
							User.findOne(
								{ 
									_id : new ObjectId(request.initiator) 
								}
							).done(
								function(err, user) {

									if (err) {
					    				console.log("Error finding user: "+JSON.stringify(err));
					  					return res.json(
				  							{
				  								success: false,
				  								message: "Error finding user"
				  							}
				  						);
					  				} else {

					  					// Creating notifications of Invest activity for 
					  					// all followers of the user/initiator
					  					for (var i=0; i < user.followers.length; i++) {
								        	var follower = user.followers[i];
							 		    	console.log("Creating Notification for: " + follower);
							 		    	
							 		    	var newNotification = {};
						    				newNotification.userId = follower;
											newNotification.collection = "Activity";
											newNotification.collectionId = persistedActivity.id;
											
											var notificationRequest = {};
											notificationRequest.body = newNotification;
											NotificationController.create(notificationRequest, res);
								        }
					  				}
								}
							);
						}
						// We will not create notifications for the Invest activity, since notifications
						// for followers of the fund as well as investors have already been handled in
						// the above code
	    				if(persistedActivity.activityType != 'Invest'){

	    					console.log("Creating Notification for User: "+ newNotification.userId);

	    					newNotification.collection = 'Activity';
							newNotification.collectionId = persistedActivity.id;

							req.body = newNotification;
							NotificationController.create(req,res);

	    				}
						return res.json(
  							true,
  							201
  						);
	  				}
				}
			);
		}
	},
	// Remove an activity
	destroy : function(req, res) {

		var queryJson = {};
		var removeNotificationQueryJson = {};
		
		if(req.param){
			var id = req.param('id');
			queryJson = {
	  					_id: new ObjectID(id)
	  				};

	  		removeNotificationQueryJson.collection = "Activity";
	  		removeNotificationQueryJson.collectionId = id;
	  									
		} else {
			queryJson = req.queryJson;
		}

		Activity.native(
			function(err,collection){
	  			collection.remove(
	  				queryJson,
	  				1,
	  				function(err,removedActivity){
	  					if(err){
	  						// The Activity could not be Removed!
		    				console.log("Error Removing Activity: "+err);
		  					return res.json(
	  							{
	  								success: false,
	  								message: "Error Removing Activity"
	  							}
	  						);
	  					}
	  					else {

	  						if(removedActivity > 0) {

	  							// Removing Notifications related to the Activity
	  							if(removeNotificationQueryJson.collectionId) {
	  								var removeEntityRequest = {};
									removeEntityRequest.queryJson = removeNotificationQueryJson;
								
									sails.controllers.notification.destroy(removeEntityRequest,res);
	  							}
	  							
								// Activity was successfully Removed!
			    				console.log("Success Removing Activity: " + removedActivity);
			  					return res.json(
		  							{
		  								success: true,
		  								message: "Success Removing Activity"
		  							}
		  						);
	  						} else {
	  							// No Activity removed/found!
			    				console.log("No Activity removed/found");
			  					return res.json(
		  							{
		  								success: false,
		  								message: " No Activity removed/Found"
		  							}
		  						);
	  						}
	  					}
	  				}
	  			);
	  		}
		);
	},

	fetchLikes: function(req, res) {

		var request = req.body;
	    var parentId = req.param("parentId");
	    var parentCollection = req.param("collection");

	    // Paginating the followers list using page number and 
	    // number of documents per page
	    var pageSize = req.param("size");
	    var page = pageSize;
	    var pageNumber = req.param("page");

	    // Calculating the number of documents to skip
	    var skip = pageSize * (pageNumber - 1);

	    // Fetching Likes
        Activity.native(
          	function(err,collection){
	            if(err){
	              console.log("Error making native query: "+JSON.stringify(err));
	              return res.json(
	                {
	                  success: false,
	                  message: "Error making native query"
	                }
	              );
	            } else {
	              	collection.find(
	              		{
	              			parentId : parentId,
	              			parentCollection : parentCollection,
	              			activityType : "Like"
	              		}
	              	)
	              	.skip(skip)
	              	.limit(parseInt(pageSize))
	            	.sort({_id: 1}) // Sort by Id
		            .toArray(
		                function(err, likes){
		                  if(err){
		                    // Likes could not be fetched!
		                    console.log("Error Fetching Likes: "+ JSON.stringify(err));
		                    return res.json(
		                    	{
		                      		success: false,
		                     		 message: "Error Fetching Likes"
		                    	}
		                  	);
		                  } else {
		                    
		                  	var counter = 0;

		                    for(var i=0; i < likes.length; i++){
		                    	sails.controllers.user.findName(
		                    		likes[i].initiator,
		                    		function(name){
		                    			
		                    			likes[counter].initiator_name = name;
		                    			counter++;

		                    			if(counter == likes.length){
		                    				return res.json( 
						                    	likes,
						                        200
						                    );
		                    			}
		                    		}
		                    	)
		                    }
		                  }
		                }
		            );
	            }
          	}
        );
	},

	fetchShares: function(req, res) {

		var request = req.body;
	    var parentId = req.param("parentId");

	    // Paginating the followers list using page number and 
	    // number of documents per page
	    var pageSize = req.param("size");
	    var page = pageSize;
	    var pageNumber = req.param("page");

	    // Calculating the number of documents to skip
	    var skip = pageSize * (pageNumber - 1);

	    // Fetching Shares
        Activity.native(
          	function(err,collection){
	            if(err){
	              console.log("Error making native query: "+JSON.stringify(err));
	              return res.json(
	                {
	                  success: false,
	                  message: "Error making native query"
	                }
	              );
	            } else {
	              	collection.find(
	              		{
	              			parentId : parentId,
	              			parentCollection : "Post",
	              			activityType : "Share"
	              		}
	              	)
	              	.skip(skip)
	              	.limit(parseInt(pageSize))
	            	.sort({_id: 1}) // Sort by Id
		            .toArray(
		                function(err, shares){
		                  if(err){
		                    // Shares could not be fetched!
		                    console.log("Error Fetching Shares: "+ JSON.stringify(err));
		                    return res.json(
		                    	{
		                      		success: false,
		                     		 message: "Error Fetching Shares"
		                    	}
		                  	);
		                  } else {

		                  	var counter = 0;

		                    for(var i=0; i < shares.length; i++){
		                    	sails.controllers.user.findName(
		                    		shares[i].initiator,
		                    		function(name){
		                    			
		                    			shares[counter].initiator_name = name;
		                    			counter++;

		                    			if(counter == shares.length){
		                    				return res.json( 
						                    	shares,
						                        200
						                    );
		                    			}
		                    		}
		                    	)
		                    }
		                  }
		                }
		            );
	            }
          	}
        );
	},

	fetchInvestments: function(req, res) {

		var request = req.body;
	    var parentId = req.param("parentId");

	    // Paginating the followers list using page number and 
	    // number of documents per page
	    var pageSize = req.param("size");
	    var page = pageSize;
	    var pageNumber = req.param("page");

	    // Calculating the number of documents to skip
	    var skip = pageSize * (pageNumber - 1);

	    // Fetching Investments
        Activity.native(
          	function(err,collection){
	            if(err){
	              console.log("Error making native query: "+JSON.stringify(err));
	              return res.json(
	                {
	                  success: false,
	                  message: "Error making native query"
	                }
	              );
	            } else {
	              	collection.find(
	              		{
	              			parentId : parentId,
	              			parentCollection : "Fund",
	              			activityType : "Invest"
	              		}
	              	)
	              	.skip(skip)
	              	.limit(parseInt(pageSize))
	            	.sort({_id: 1}) // Sort by Id
		            .toArray(
		                function(err, investments){
		                  if(err){
		                    // Investments could not be fetched!
		                    console.log("Error Fetching Investments: "+ JSON.stringify(err));
		                    return res.json(
		                    	{
		                      		success: false,
		                     		message: "Error Fetching Investments"
		                    	}
		                  	);
		                  } else {
		                    
		                    var counter = 0;

		                    for(var i=0; i < investments.length; i++){
		                    	sails.controllers.user.findName(
		                    		investments[i].initiator,
		                    		function(name){
		                    			
		                    			investments[counter].initiator_name = name;
		                    			counter++;

		                    			if(counter == investments.length){
		                    				return res.json( 
						                    	investments,
						                        200
						                    );
		                    			}
		                    		}
		                    	)
		                    }
		                  }
		                }
		            );
	            }
          	}
        );
	},

	fetchReferrals: function(req, res) {

		var request = req.body;
	    var parentId = req.param("parentId");
	    var parentCollection = req.param("collection");

	    // Paginating the followers list using page number and 
	    // number of documents per page
	    var pageSize = req.param("size");
	    var page = pageSize;
	    var pageNumber = req.param("page");

	    // Calculating the number of documents to skip
	    var skip = pageSize * (pageNumber - 1);

	    // Fetching Referrals
        Activity.native(
          	function(err,collection){
	            if(err){
	              console.log("Error making native query: "+JSON.stringify(err));
	              return res.json(
	                {
	                  success: false,
	                  message: "Error making native query"
	                }
	              );
	            } else {
	              	collection.find(
	              		{
	              			parentId : parentId,
	              			parentCollection : parentCollection,
	              			activityType : "Refer"
	              		}
	              	)
	              	.skip(skip)
	              	.limit(parseInt(pageSize))
	            	.sort({_id: 1}) // Sort by Id
		            .toArray(
		                function(err, referrals){
		                  if(err){
		                    // Referrals could not be fetched!
		                    console.log("Error Fetching Referrals: "+ JSON.stringify(err));
		                    return res.json(
		                    	{
		                      		success: false,
		                     		message: "Error Fetching Referrals"
		                    	}
		                  	);
		                  } else {
		                    
		                  	var counter = 0;

		                    for(var i=0; i < referrals.length; i++){
		                    	sails.controllers.user.findName(
		                    		referrals[i].initiator,
		                    		function(name){
		                    			
		                    			referrals[counter].initiator_name = name;
		                    			counter++;

		                    			if(counter == referrals.length){
		                    				return res.json( 
						                    	referrals,
						                        200
						                    );
		                    			}
		                    		}
		                    	)
		                    }
		                  }
		                }
		            );
	            }
          	}
        );
	},

	getDetails : function (id,callback) {

		Activity.native(
	      function(err, collection){
	        collection.findOne(
	          {
	            _id: new ObjectId(id)
	          },
	          function(err, activity){
	            if(err){
	                // Activity could not be found!
	                console.log("Error finding Activity: " + JSON.stringify(err));
	                
	            } else {
	              	sails.controllers.user.findName(
	              		activity.initiator,
	              		function(initiatorName){
	              			
	              			if(initiatorName){

	              				console.log("initiated: "+ initiatorName);

	              				activity.E1 = initiatorName;

	              				if(activity.parentIdInitiator){

	              					sails.controllers.user.findName(
			              				activity.parentIdInitiator,
			              				function(parentIdInitiatorName){
			              					if(parentIdInitiatorName){
			              						console.log("ParentId Initiator Name: "+parentIdInitiatorName);
			              						activity.E2 = parentIdInitiatorName;
			              						callback(activity);

			              					} else {
			              						console.log("Parent not Found!");
			              					}
			              				}
			              			);

	              				} else if(activity.initiated){

	              					sails.controllers.user.findName(
			              				activity.initiated,
			              				function(initiatedName){
			              					if(initiatedName){
			              						console.log("Parent Name: "+initiatedName);
			              						activity.E2 = initiatedName;

			              						callback(activity);

			              					} else {
			              						console.log("Parent not Found!");
			              					}
			              				}
			              			);

	              				} else {

	              					if(activity.parentCollection == "User"){

	              						sails.controllers.user.findName(
				              				activity.parentId,
				              				function(parentName){
				              					if(parentName){
				              						console.log("Parent Name: "+parentName);
				              						activity.E2 = parentName;

				              						callback(activity);

				              					} else {
				              						console.log("Parent not Found!");
				              					}
				              				}
				              			);
	              					
	              					} else {

	              						sails.controllers.fund.findName(
				              				activity.parentId,
				              				function(parentName){
				              					if(parentName){
				              						console.log("Parent Name: "+parentName);
				              						activity.E2 = parentName;

				              						callback(activity);

				              					} else {
				              						console.log("Parent not Found!");
				              					}
				              				}
				              			);

	              					}
	              				}

	              			} else {
	              				console.log("Initiator Not Found for Activity");
	              				return initiatorName;
	              			}
	              		}
	              	);
	            }
	          }
	        )
	      }
	    );
	}
};